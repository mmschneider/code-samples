/* B160489 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <limits.h>

#define N 729
#define reps 100
#include <omp.h> 

double a[N][N], b[N][N], c[N];
int jmax[N];  

struct check{
  int * lo_values;
  int * hi_values;
};

void init1(void);
void init2(void);
void runloop(int, struct check *); 
void loop1chunk(int, int);
void loop2chunk(int, int);
void valid1(void);
void valid2(void);

int main(int argc, char *argv[]) { 
  double start1,start2,end1,end2;
  int r;
  struct check check;
  
  init1(); 
  
  start1 = omp_get_wtime(); 

  for (r=0; r<reps; r++){ 
    runloop(1, &check);
  } 

  end1  = omp_get_wtime();  

  valid1(); 

  printf("Total time for %d reps of loop 1 = %f\n",reps, (float)(end1-start1)); 


  init2(); 

  start2 = omp_get_wtime(); 

  for (r=0; r<reps; r++){ 
    runloop(2, &check);
  } 

  end2  = omp_get_wtime(); 

  valid2(); 

  printf("Total time for %d reps of loop 2 = %f\n",reps, (float)(end2-start2)); 

} 

void init1(void){
  int i,j; 

  for (i=0; i<N; i++){ 
    for (j=0; j<N; j++){ 
      a[i][j] = 0.0; 
      b[i][j] = 3.142*(i+j); 
    }
  }

}

void init2(void){ 
  int i,j, expr; 

  for (i=0; i<N; i++){ 
    expr =  i%( 3*(i/30) + 1); 
    if ( expr == 0) { 
      jmax[i] = N;
    }
    else {
      jmax[i] = 1; 
    }
    c[i] = 0.0;
  }

  for (i=0; i<N; i++){ 
    for (j=0; j<N; j++){ 
      b[i][j] = (double) (i*j+1) / (double) (N*N); 
    }
  }
 
} 


void runloop(int loopid, struct check * check)  {

  #pragma omp parallel default(none) shared(loopid, check) 
  {
    int update, run_switch, min_id, min_seen, i, num_left;
    int myid  = omp_get_thread_num();
    int nthreads = omp_get_num_threads(); 
    int ipt = (int) ceil((double)N/(double)nthreads); 
    int lo = myid*ipt;
    int hi = (myid+1)*ipt;
    if (hi > N) hi = N; 
    float frac = 1/(float)nthreads;
    int next_thread = myid;

    // Allocate memory to the check struct based on the number of threads. 
   #pragma omp single
   {   
     check->lo_values = malloc(nthreads * sizeof(*check->lo_values));
     check->hi_values = malloc(nthreads * sizeof(*check->hi_values));
   }
   
    // Assign  the values of the check struct.
    check->lo_values[myid] = lo;
    check->hi_values[myid] = hi;
    #pragma omp barrier
  
    while(next_thread != -1)
    {
      run_switch = 0;
      /* Read current values in check struct and update to what this thread
         will computer so two threads won't work on the same thing.*/
      #pragma omp critical
      {
       if(next_thread != myid || check->lo_values[myid] >= check->hi_values[myid])
        {
          min_id = -1;
          min_seen = INT_MAX;
          for(i = 0; i < nthreads; i++)
          {
            num_left = check->hi_values[i] - check->lo_values[i];
            if(num_left < min_seen && num_left != 0)
            {
              min_seen = num_left;
              min_id = i;
             }
          }
          next_thread = min_id;
        }
        if(min_id != -1){
          // Get the most up to date lo and hi values.
          hi = check->hi_values[next_thread];
          lo = check->lo_values[next_thread];
          // Only make changes if other thread did not complete all chunks.
          if(lo < hi)
          { 
            run_switch = 1;
            int num_remaining = hi - lo;
            update = (int) num_remaining*(frac);
            if (update < 1) update = 1;
            check->lo_values[next_thread] = lo + update;
          }
        }
      }  
      if(run_switch)
      {
        switch (loopid) 
        { 
          case 1: loop1chunk(lo,lo+update); break;
          case 2: loop2chunk(lo,lo+update); break;
        } 
      }

    }
  }
 
 free(check->lo_values);
 free(check->hi_values);
}

void loop1chunk(int lo, int hi) { 
  int i,j; 
  
  for (i=lo; i<hi; i++){ 
    for (j=N-1; j>i; j--){
      a[i][j] += cos(b[i][j]);
    } 
  }

} 



void loop2chunk(int lo, int hi) {
  int i,j,k; 
  double rN2; 

  rN2 = 1.0 / (double) (N*N);  

  for (i=lo; i<hi; i++){ 
    for (j=0; j < jmax[i]; j++){
      for (k=0; k<j; k++){ 
	c[i] += (k+1) * log (b[i][j]) * rN2;
      } 
    }
  }

}

void valid1(void) { 
  int i,j; 
  double suma; 
  
  suma= 0.0; 
  for (i=0; i<N; i++){ 
    for (j=0; j<N; j++){ 
      suma += a[i][j];
    }
  }
  printf("Loop 1 check: Sum of a is %lf\n", suma);

} 


void valid2(void) { 
  int i; 
  double sumc; 
  
  sumc= 0.0; 
  for (i=0; i<N; i++){ 
    sumc += c[i];
  }
  printf("Loop 2 check: Sum of c is %f\n", sumc);
} 
 

