#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#include "percolate.h"
#include "lib/initialize.h"
#include "lib/finalize.h"
#include "lib/create_datatypes.h"
#include "lib/get_time.h"
#include "lib/halo_swap.h"
#include "lib/io.h"
#include "lib/neighbors.h"
#include "lib/reduce.h"
#include "lib/update_new.h"

int main(int argc, char *argv[]) {
 
  if (argc != 2 && argc != 3) {
    printf("Usage: percolate <seed> <L (optional)> \n");
    return 1;
  }
  if(argc == 3) L = atoi(argv[2]);
  else L = 288;

  /* struct holding information about the processors. */

  Parallel_Info parallel_info;

  /* Struct holding information about the startinf and stop times. */ 

  Time_Info time;

  if(initialize(&parallel_info, &time) < 0) return -1; 
  
  /* 
   * Get all four of each processors neighbors and store that information in neighbors. 
   * This also sets the values of M and N based on the decomposition.
   */

  neighbors_t neighbors;
  get_neighbors(&neighbors, &parallel_info, &M, &N);

  create_datatypes();

  /*
   *  Define the main arrays for the simulation.
   */

  int old[M + 2][N + 2], new[M + 2][N + 2];

  /*
   *  Additional array WITHOUT halos for initialisation and IO. This
   *  is of size LxL because, even in our parallel program, we do
   *  these two steps in serial.
   */

  int map[L][L];

  /*
   *  Variables that define the simulation.
   */

  int seed;
  double rho;

  /*
   *  Local variables.
   */

  int i, j, nhole, step, continue_searching, nchange, printfreq;
  int itop, ibot, perc;
  double r;
  long total_sum, local_sum;

  /*
   *  Set most important value: the rock density rho (between 0 and 1).
   */

  rho = 0.411;

  /*
   *  Set the randum number seed and initialise the generator
   */

  seed = atoi(argv[1]);
  time.serial_start1 = get_time();

  if (parallel_info.rank == 0) {
    printf("percolate: params are L = %d, rho = %f, seed = %d\n", L, rho, seed);

    rinit(seed);

    /*
     *  Initialise map with density rho. Zero indicates rock, a positive
     *  value indicates a hole. For the algorithm to work, all the holes
     *  must be initialised with a unique integer.
     */

    nhole = 0;

    for (i = 0; i < L; i++) {
      for (j = 0; j < L; j++) {
        r = uni();

        if (r < rho) {
          map[i][j] = 0;
        } else {
          nhole++;
          map[i][j] = nhole;
        }
      }
    }

    printf("percolate: rho = %f, actual density = %f\n", rho,
           1.0 - ((double)nhole) / ((double)L * L));
  }
  
  time.serial_stop1 = get_time();

  /*
   * Processor 0 sends map sections to other processors.
   */

  scatter(parallel_info, map, old);
   
  /*
   * Initialize halos of old.
   */

  for (i = 0; i <= M + 1; i++) // zero the bottom and top halos
  {
    old[i][0] = 0;
    old[i][N + 1] = 0;
  }

  for (j = 0; j <= N + 1; j++) // zero the left and right halos
  {
    old[0][j] = 0;
    old[M + 1][j] = 0;
  }

  printfreq = 100;

  step = 1;
  nchange = 1;
  continue_searching = 1;

  while (continue_searching) {

    /*
     * Send information to neighbors and compute updates.
     * Return number of changes for this iteraiton. 
     */
 
    nchange = halo_swap(old, new, neighbors, parallel_info.grid);

 
    /*
     *  Report progress every now and then.
     */

    if (step != 1 && (step-1)  % printfreq == 0) {
      if (parallel_info.rank == 0) {
        printf("percolate: number of changes on step %d is %d. The current "
               "average is %lf.\n",
               step - 1, continue_searching, (double)total_sum / (double)nhole);
      }
    }

    /*
     *  Copy back in preparation for next step, omitting halos.
     */
    
    local_sum = 0;
    for (i = 1; i <= M; i++) {
      for (j = 1; j <= N; j++) {
        old[i][j] = new[i][j];
        local_sum += old[i][j];
      }
    }

    if (step % printfreq == 0) {

      /* 
       * Add all processors values of nchange and set continue_searching equal to that. 
       * Do the same for the local sums into the total sum.
       */
 
      reduce(&continue_searching, &nchange, &local_sum, &total_sum);    
    }
    step++;
  }

  /*
   *  All processors send their computed sections back to processor 0.
   */ 

  gather(parallel_info, map, old);

  time.serial_start2 = get_time();

  if (parallel_info.rank == 0) {
    
    /*
     *  Test to see if percolation occurred by looking for positive numbers
     *  that appear on both the top and bottom edges.
     */

    perc = 0;

    for (itop = 0; itop < L; itop++) {
      if (map[itop][L - 1] > 0) {
        for (ibot = 0; ibot < L; ibot++) {
          if (map[itop][L - 1] == map[ibot][0]) {
            perc = 1;
          }
        }
      }
    }

    if (perc != 0) {
      printf("percolate: cluster DOES percolate\n");
    } else {
      printf("percolate: cluster DOES NOT percolate\n");
    }

    /*
     *  Write the map to the file "map.pgm", displaying only the very
     *  largest cluster (or multiple clusters if exactly the same size).
     *  If the last argument here was 2, it would display the largest 2
     *  clusters etc.
     */

    percwrite("map.pgm", map, 8);
  }
  time.serial_stop2 = get_time();

  finalize(parallel_info, time);

  return 0;
}
