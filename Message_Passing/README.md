Percolate

How to Compile Code
- run make
  - This will run the make file in the lib library and then use those executables to build percolate.

How to Run Code
- mpirun -n {number of processors} ./percolate <seed> <L(optional)>
  - Default for L is not give is 288.
  - To change rho, the source file percolate.c must be updated and percolate must be recompiled.

Restriction on Running Code
- The number of processors must be able to split such that:
  - p1 x p2 = p
  - L % p1 = 0 and L % p2 = 0
- The code will do this split for you and it is generally safe to choose a p such that L % p = 0.

Structure 
- Main directory 
  - This is where the code should be compiled and run from.
  - Output from the code will appear in this directory (map.pgm).
  - Contents:
    - percolate.c and percolate.h
    - README.md
    - Make file
    - uni.c and uni. h
      - For random number functionality.

- lib directory 
  - All of the MPI functionality comes from the files in this directory.
  - Contents:
    - initialize.c and initialize.h
      - Contains MPI_Init and other initialization requirements.
    - create\_datatypes.c and create\_datatypes.h
      - For creating MPI derived datatypes
    - get\_time.c and get\_time.h
      - For getting the time at a particular spot in the code.
    - halo\_swap.c and halo\_swap.h
      - For performing the halo swap functionality in percolate.
    - io.c anf io.h
      - For performing scatter and gather functionality.
    - neighbors.c and neighbors.h
      - For finding each ranks neighbor and setting up a cartision communicator.
    - reduce.c and reduce.h
      - The reduction functions for keeping track of number of changes and map averages.
    - update\_new.c and update\_new.h
      - Performs the updates to the map  to create clusters.
    - finalize.c and finalize.h
      - Calls MPI_Finalize and frees derived datatypes. Prints final time information.
    - Makefile
