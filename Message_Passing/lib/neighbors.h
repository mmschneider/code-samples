#include <mpi.h>
#include "initialize.h"

#ifndef neighbors_h
#define neighbors_h

typedef struct neighbors_t{
        int forward_neighbor;
        int backward_neighbor;
        int above_neighbor;
        int below_neighbor;
}  neighbors_t;

#endif

int get_neighbors(neighbors_t * neighbors, Parallel_Info * parallel_info,int *M, int *N);
