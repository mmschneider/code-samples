#include <stdio.h>
#include "../percolate.h"
#include "finalize.h"
#include "create_datatypes.h"

int finalize(Parallel_Info parallel_info, Time_Info time){
  MPI_Type_free(&MPI_GRID);
  MPI_Type_free(&MPI_ROW);
  MPI_Type_free(&MPI_OLD);

  time.tstop = get_time();

  double total_time = time.tstop - time.tstart;
  double serial_time = time.serial_stop1 - time.serial_start1 + (time.serial_stop2 - time.serial_start2);
  double parallel_time = total_time - serial_time;
  if(parallel_info.rank == 0)
  fprintf( stderr, "Total Time: %f Serial Time: %f Parallel Time: %f \n", total_time, serial_time, parallel_time);

  MPI_Finalize();

}
