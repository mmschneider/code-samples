#include <mpi.h>

#include "get_time.h"

double get_time(){
  MPI_Barrier(MPI_COMM_WORLD);	
  return MPI_Wtime();
}
