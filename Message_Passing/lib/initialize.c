#include "initialize.h"
#include <stdio.h>

int initialize(Parallel_Info * parallel_info, Time_Info * time){  
  
 MPI_Init(NULL, NULL);
 
  time->tstart = get_time();

  parallel_info->dims[0]=0;
  parallel_info->dims[1]=0;
 
  MPI_Comm_rank(MPI_COMM_WORLD, &(parallel_info->rank));
  MPI_Comm_size(MPI_COMM_WORLD, &(parallel_info->size));
  MPI_Dims_create(parallel_info->size, 2, parallel_info->dims);
  
  /*
   * Ensure an appropriate number of threads is being used.
   */

  if (L % parallel_info->dims[0] != 0 || L % parallel_info->dims[1] != 0) {
    if (parallel_info->rank == 0) {
      printf(
          "Incompatible number of processors. Consider a factor of L: %d. \n",
          L);
    }
    MPI_Finalize();
    return -1;
  }


  return 0;

}

