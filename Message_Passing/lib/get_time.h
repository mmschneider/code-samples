
#ifndef get_time_h
#define get_time_h

typedef struct Time_Info{
        double tstart;
        double tstop;
        double serial_start1;
        double serial_start2;
        double serial_stop1;
        double serial_stop2;
}       Time_Info;

#endif

double get_time();
