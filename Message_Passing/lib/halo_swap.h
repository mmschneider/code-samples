#include <mpi.h>

#include "../percolate.h"
#include "neighbors.h"

int halo_swap(int old[M+2][N+2], int new[M+2][N+2], neighbors_t neighbors, MPI_Comm grid);
