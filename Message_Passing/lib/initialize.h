#include <mpi.h>
#include "../percolate.h"
#include "get_time.h"

#ifndef initialize_h
#define initialize_h

typedef struct Parallel_Info{
	int size;
	int rank;
	int dims[2];
	MPI_Comm grid;
}	Parallel_Info;

#endif


int initialize(Parallel_Info * parallel_info, Time_Info * time);
