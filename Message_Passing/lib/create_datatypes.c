#include "../percolate.h"
#include "create_datatypes.h"


int create_datatypes(){

  /*
   * Create vector type for 2D domain decomposition.
   */

  MPI_Type_vector(M, N, L, MPI_INT, &MPI_GRID);
  MPI_Type_commit(&MPI_GRID);

  /*
   * Create vector type to allow copying straight into old array.
   */

  MPI_Type_vector(M, N, N + 2, MPI_INT, &MPI_OLD);
  MPI_Type_commit(&MPI_OLD);

  /*
   * Create vector type to access row in old array.
   */

  MPI_Type_vector(M, 1, N + 2, MPI_INT, &MPI_ROW);
  MPI_Type_commit(&MPI_ROW);
  
  return 0;
}
