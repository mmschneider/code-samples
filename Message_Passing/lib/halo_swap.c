#include "halo_swap.h"
#include "create_datatypes.h"

int halo_swap(int old[M+2][N+2], int new[M+2][N+2],  neighbors_t neighbors, MPI_Comm grid){
    int nchange, i, j;
    MPI_Request halo_request[4];    
    MPI_Status halo_status[4]; 

    MPI_Isend(&old[M][1], N, MPI_INT, neighbors.forward_neighbor, 0, grid,&halo_request[0]);
    MPI_Isend(&old[1][1], N, MPI_INT, neighbors.backward_neighbor, 0, grid, &halo_request[1]);
    MPI_Isend(&old[1][N], 1, MPI_ROW, neighbors.above_neighbor, 0, grid, &halo_request[2]);
    MPI_Isend(&old[1][1], 1, MPI_ROW, neighbors.below_neighbor, 0, grid, &halo_request[3]);

    MPI_Recv(&old[0][1], N, MPI_INT, neighbors.backward_neighbor, 0, grid, &halo_status[0]);
    MPI_Recv(&old[M + 1][1], N, MPI_INT, neighbors.forward_neighbor, 0, grid, &halo_status[1]);
    MPI_Recv(&old[1][0], 1, MPI_ROW, neighbors.below_neighbor, 0, grid, &halo_status[2]);
    MPI_Recv(&old[1][N + 1], 1, MPI_ROW, neighbors.above_neighbor, 0, grid, &halo_status[3]);

    nchange = 0;

    /*
     * Update values
     */

    for (i = 1; i < M + 1; i++) {
      for (j = 1; j < N + 1; j++) {
        nchange += update_new(i, j, old, new);
      }
    }
   
    MPI_Waitall(4, halo_request, halo_status);
    return nchange;
}
