#include "neighbors.h"
#include <stdio.h>
#include "../percolate.h"

int get_neighbors(neighbors_t * neighbors, Parallel_Info * parallel_info, int *M, int *N){
  
  int periods[2] = {1, 0};
  int forward, backward, above, below;
  
  MPI_Cart_create(MPI_COMM_WORLD, 2, parallel_info->dims, periods, 0, &parallel_info->grid);
  MPI_Cart_shift(parallel_info->grid, 0, 1, &backward, &forward);
  MPI_Cart_shift(parallel_info->grid, 1, 1, &below, &above);

  *M = L / parallel_info->dims[0];
  *N = L / parallel_info->dims[1];

  neighbors->forward_neighbor = forward;
  neighbors->backward_neighbor = backward;
  neighbors->above_neighbor = above;
  neighbors->below_neighbor = below;

  return 0;

}
