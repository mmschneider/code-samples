#include <mpi.h>

#include "reduce.h"

int reduce(int * continue_searching, int * nchange, long * local_sum, long * total_sum){
      
      /*
       * Reduce number of changes to continue_searching so while loop exits if
       * no changes. Also keeps track of number of change to print on printfreq
       * intervals.
       */

      MPI_Allreduce(nchange, continue_searching, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

      /*
       * Reduce local sums to compue average of total map and print on printfreq
       * intervals.
       */

      MPI_Reduce(local_sum, total_sum, 1, MPI_LONG, MPI_SUM, 0,  MPI_COMM_WORLD);

}
