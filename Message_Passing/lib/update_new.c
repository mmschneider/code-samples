#include "update_new.h"

int update_new(int i, int j, int old[M+2][N+2], int new[M+2][N+2]){

  int oldval, newval, nchange;
  nchange = 0;

  oldval = old[i][j];
  newval = oldval;

  /*
   * Set new[i][j] to be the maximum value of old[i][j]
   * and its four nearest neighbours
   */

  if (oldval != 0) {
    if (old[i - 1][j] > newval) newval = old[i - 1][j];
    if (old[i + 1][j] > newval) newval = old[i + 1][j];
    if (old[i][j - 1] > newval) newval = old[i][j - 1];
    if (old[i][j + 1] > newval) newval = old[i][j + 1];

    if (newval != oldval) {
      nchange += 1;
    }
  }

  new[i][j] = newval;
  return nchange;

}
