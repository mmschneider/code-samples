#include <mpi.h>
#include "../percolate.h"
#include "initialize.h"

int scatter(Parallel_Info parallel_info, int map[L][L], int old[M+2][N+2]);
int gather(Parallel_Info parallel_info, int map[L][L], int old[M+2][N+2]);
