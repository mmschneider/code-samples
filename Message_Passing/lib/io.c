#include "io.h"
#include "create_datatypes.h"
#include <stdio.h>

int scatter(Parallel_Info parallel_info, int map[L][L], int old[M+2][N+2]){
  int i, j;
 
  MPI_Request request[parallel_info.size];
  /*
  * Rank 0 sends individual 2D decomposition components to each rank
  */
  if (parallel_info.rank == 0) {
   int send_rank;
   int cords[2];
   int index = 0;
   for (i = 0; i < L; i += M) {
      for (j = 0; j < L; j += N) {      
        cords[0] = i / M;
        cords[1] =  j / N;
        MPI_Cart_rank(parallel_info.grid, cords, &send_rank);
        MPI_Isend(&map[i][j], 1, MPI_GRID, send_rank, 0, MPI_COMM_WORLD, &request[index]);
        index++;
      }
    }
  }
  MPI_Recv(&old[1][1], 1, MPI_OLD, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);


  if (parallel_info.rank == 0) {
    MPI_Waitall(parallel_info.size, request, MPI_STATUSES_IGNORE);
  }
  return 0;

}

int gather(Parallel_Info parallel_info, int map[L][L], int old[M+2][N+2]){
  int i,j;
  MPI_Request request[parallel_info.size];
 /*
  * Send updated arrays back to rank 0 to create finished map.
  */

  if (parallel_info.rank == 0) {
    for (i = 0; i < parallel_info.size; i++) {
      int cords[2];
      MPI_Cart_coords(parallel_info.grid, i, 2, cords);
      MPI_Irecv(&map[M * cords[0]][N * cords[1]], 1, MPI_GRID, i, 0, MPI_COMM_WORLD, &request[i]);
    }
  }

  MPI_Send(&old[1][1], 1, MPI_OLD, 0, 0, MPI_COMM_WORLD);

  if (parallel_info.rank == 0) {
    MPI_Waitall(parallel_info.size, request, MPI_STATUSES_IGNORE);
  }
}
