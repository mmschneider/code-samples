from numpy import genfromtxt
from matplotlib import pyplot as plt
import numpy as np
import csv

X = genfromtxt('NSL-KDD/mod_train+_pd.csv', delimiter=',', skip_header=1)
Y = np.load('NSL-KDD/mod_train+_labels.npy')
feature_labels  = next(csv.reader(open('NSL-KDD/mod_train+_pd.csv')))
mute_mask = [0,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,101,102,103,104,109,110,111,112,113,114,115,116,117]


X_mal_index = np.where(Y==1)[0]
X_mal = np.take(X, X_mal_index, axis=0)
X_norm_index = np.where(Y==0)[0]
X_norm = np.take(X, X_norm_index, axis=0)


cols = 122
in_data_GA = np.full(cols,0.0)
in_data_GAN = np.full(cols,0.0)
in_data_PSO = np.full(cols,0.0)
in_data_FE = np.full(cols,0.0)

#GAN
file_name_GAN = "gan_results/gan_output_vectors_NSL.csv"
data_GAN = genfromtxt(file_name_GAN, dtype=float, delimiter=',')
in_data_GAN = np.vstack([in_data_GAN,data_GAN])

#GA
start_idx_GA = 222780
file_name_GA = "ga_results/ga_vec_output"
#PSO
start_idx_PSO = 2254521
file_name_PSO = "pso_results/pso_vec_output"
#FE
start_idx_FE = 16973
file_name_FE = "fe_results/fe_vec_output"

num_files = 64
for x in range(num_files):
	idx = start_idx_GA + x
	f = file_name_GA + '_' + str(idx) + '.csv'
	data_GA = genfromtxt(f, dtype=float, delimiter=',')
	in_data_GA = np.vstack([in_data_GA,data_GA])

for x in range(num_files):
	idx = start_idx_PSO + x
	f = file_name_PSO + '_' + str(idx) + '.csv'
	data_PSO = genfromtxt(f, dtype=float, delimiter=',')
	in_data_PSO = np.vstack([in_data_PSO,data_PSO])

num_files_fe = 8
for x in range(num_files_fe):
    idx = start_idx_FE + x
    f = file_name_FE + '_' + str(idx) + '.csv'
    data_FE = genfromtxt(f, dtype=float, delimiter=',')
    in_data_FE = np.vstack([in_data_FE,data_FE])

in_data_GA = np.delete(in_data_GA,0,0)
in_data_GAN = np.delete(in_data_GAN,0,0)
in_data_PSO = np.delete(in_data_PSO,0,0)
in_data_FE = np.delete(in_data_FE,0,0)


for feature_index in mute_mask:
	feature = feature_labels[feature_index]
	feature_orig_mal = X_mal[:,feature_index]
	feature_orig_norm = X_norm[:,feature_index]
	feature_adv_GA = in_data_GA[:,feature_index]
	feature_adv_GAN = in_data_GAN[:,feature_index]
	feature_adv_PSO = in_data_PSO[:,feature_index]
	feature_adv_FE = in_data_FE[:,feature_index]	
	y_GAN = np.full(len(feature_adv_GAN), "GAN")
	y_GA = np.full(len(feature_adv_GA), "GA")
	y_PSO = np.full(len(feature_adv_PSO), "PSO")
	y_FE = np.full(len(feature_adv_FE), "FE")
	y_mal = np.full(len(feature_orig_mal), "Malicious")
	y_norm = np.full(len(feature_orig_norm), "Normal")
	fig = plt.figure()
	plt.scatter(feature_adv_GAN, y_GAN, c='black');
	plt.scatter(feature_adv_PSO, y_PSO, c='black');
	plt.scatter(feature_adv_GA, y_GA, c='black');
	plt.scatter(feature_adv_FE, y_FE, c='black');
	plt.scatter(feature_orig_mal, y_mal, c='red');
	plt.scatter(feature_orig_norm, y_norm, c='green');
	plt.savefig("../figures/"+feature, bbox_inches="tight")
