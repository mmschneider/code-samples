import os
import time
import sys
import argparse
import numpy as np
from numpy import ndarray
import pandas as pd
import multiprocessing as mp
from multiprocessing import shared_memory
from sklearn.tree import DecisionTreeClassifier
from sklearn import metrics, svm
from sklearn.naive_bayes import GaussianNB, ComplementNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report, accuracy_score, confusion_matrix
from sklearn.ensemble import VotingClassifier, RandomForestClassifier, GradientBoostingClassifier, BaggingClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
from sklearn.linear_model import LogisticRegression
import logging
from numpy import genfromtxt
from sklearn.inspection import permutation_importance
from sklearn.model_selection import train_test_split
from numpy import genfromtxt
import joblib
import csv
import random


def closest_node(nodes, train_mal, shared_array, shape, d_type, start, mutable_index, measurement):
    existing_shm = shared_memory.SharedMemory(name=shared_array)
    update_array = np.ndarray(shape, dtype=d_type, buffer=existing_shm.buf)
    update_index = start
    train_mal_mute = np.take(train_mal, mutable_index, axis=1)

    for sample in nodes:
        #manhattan distance to closest malicious point
        sample_mute = np.take(sample, mutable_index)
        dist = np.sum(abs(train_mal_mute - sample_mute), axis=1)
        index_min = np.argmin(dist)
        closest_point = train_mal[index_min]
        if (measurement == "average"):
            update_array[update_index] = abs(closest_point - sample)
        elif (measurement == "rank"):
            non_mute = np.setdiff1d(np.array(range(len(closest_point))), mutable_index)
            np.put(closest_point, non_mute, 0)
            np.put(sample, non_mute, 0)
            update_array[update_index] = np.argsort(abs(closest_point - sample))

        update_index += 1
    existing_shm.close()

def dropout(X_zeroed, X, feature_labels, mutable_index):
    prob = 1.0/len(mutable_index)
    update_index=0
    for sample in X:
        j=0
        for i in mutable_index:
            if random.random() <= prob:
                X_zeroed[update_index][len(X[0])+j-1] = 1
                X_zeroed[update_index][i] = 0
            j+=1
        update_index += 1
    return(X_zeroed)

def test_dropout(model, X_test, Y_test, acc_orig, feature_labels,  X_len, mutable_index):
    j=0
    for i in mutable_index:
        X_test_copy=np.copy(X_test)
        X_test_copy[:,i]=0
        X_test_copy[:,X_len+j-1]=1
        j+=1
        print(feature_labels[i] + " " + str(acc_orig-model.score(X_test_copy, Y_test)))
           

def scikit_learn_perm(model, X, Y, feature_names, mutable_index):
    r = permutation_importance(model, X, Y)
    for i in range(len(feature_names)):
        if i in mutable_index:
            print(feature_names[i]+ ", " + str(r.importances_mean[i]))

def main(argv):

    parser = argparse.ArgumentParser(description='script to run output vectors against multiple classifiers')
    parser.add_argument('-f', '--file', help='input file name to use')
    parser.add_argument('-i', '--index', help='file pid index starting number, -1 for single file')
    parser.add_argument('-n', '--num_files', default=1, help='number of files')
    parser.add_argument('-v', '--verbose', default=5, type=int, help='debug message level [1-5]')
    parser.add_argument('-d', '--dataset', default='NSL', choices=['NSL','UNSW'],help='dataset to use: NSL or UNSW')
    parser.add_argument('-t', '--test', default=0, type=int, help='test method [0-3]')
    args = parser.parse_args()
    
    if args.verbose == 1:
        level=logging.CRITICAL
    elif args.verbose == 2:
        level=logging.ERROR
    elif args.verbose == 3:
        level=logging.WARNING
    elif args.verbose == 4:
        level=logging.INFO
    else:
        level=logging.DEBUG
    logging.basicConfig(level=level, format='%(asctime)s - %(levelname)s -%(message)s')

    if args.dataset == 'NSL':
        dset = 0
        cols = 122
        feature_labels  = next(csv.reader(open('NSL-KDD/mod_train+_pd.csv')))
        X = genfromtxt('NSL-KDD/mod_train+_pd.csv', delimiter=',', skip_header=1)
        Y = np.load('NSL-KDD/mod_train+_labels.npy')
        X_test = genfromtxt('NSL-KDD/mod_test+_pd.csv', delimiter=',', skip_header=1)
        Y_test = np.load('NSL-KDD/mod_test+_labels.npy')
    elif args.dataset == 'UNSW':
        dset = 1
        cols = 196
        feature_labels  = next(csv.reader(open('UNSW_NB15/mod_train+_pd.csv')))
        X = genfromtxt('UNSW_NB15/mod_train+_pd.csv', delimiter=',', skip_header=1)
        Y = np.load('UNSW_NB15/mod_train+_labels.npy')

    for i in range(len(X)):
        if (X[i][95]>1):
            X[i][95]=1

    start_idx = args.index
    start_idx = int(start_idx)
    num_files = int(args.num_files)
    file_name = args.file
    in_data = np.full(cols,0.0)

    if start_idx < 0:
        file_name = args.file + '.csv'
        data = genfromtxt(file_name, dtype=float, delimiter=',')
        in_data = np.vstack([in_data,data])
    else:
        for x in range(num_files):
            idx = start_idx + x
            file_name = args.file + '_' + str(idx) + '.csv'
            data = genfromtxt(file_name, dtype=float, delimiter=',')
            in_data = np.vstack([in_data,data])

    in_data = np.delete(in_data,0,0)

    #load saved models to test against
    #svm_file_name = 'Models/svm_model.sav'
    dt_file_name = 'Models/dt_model.sav'
    #nb_file_name = 'Models/nb_model.sav'
    #knn_file_name = 'Models/knn_model.sav'
    rf_file_name = 'Models/rf_model.sav'
    mlp_file_name = 'Models/mlp_model.sav'
    mlp_file_name = 'Models/mlp_model.sav'
    #gb_file_name = 'Models/gb_model.sav'
    lr_file_name = 'Models/lr_model.sav'
    lda_file_name = 'Models/lda_model.sav' 
    qda_file_name = 'Models/qda_model.sav' 
    #bag_file_name = 'Models/bag_model.sav'

    #svm_model = joblib.load(svm_file_name)
    dt_model = joblib.load(dt_file_name)
    #nb_model = joblib.load(nb_file_name)
    #knn_model = joblib.load(knn_file_name)
    rf_model = joblib.load(rf_file_name)
    mlp_model = joblib.load(mlp_file_name)
    #gb_model = joblib.load(gb_file_name)
    lr_model = joblib.load(lr_file_name)
    lda_model = joblib.load(lda_file_name)
    qda_model = joblib.load(qda_file_name)
    #bag_model = joblib.load(bag_file_name)

    model_list = [dt_model, rf_model, lr_model, lda_model, qda_model, mlp_model]
    model_list_names = ['DT','RF','LR','LDA','QDA', 'MLP'] 
    label_len = len(in_data)
    logging.debug('input file column length: %d',label_len)
    labels = np.full(label_len, 1, int)
    mutable_index = [0,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,101,102,103,104,109,110,111,112,113,114,115,116,117]

    # Feature importance scores
    if args.test == 0 or args.test == 1:
        for i in range(len(model_list)):
            logging.info('\nTest 1 ' + model_list_names[i])
            scikit_learn_perm(model_list[i], X, Y, feature_labels, mutable_index)

    # Distance metric vulnerability scores
    if args.test == 0 or args.test == 2:

        for i in range(len(model_list)):
            logging.info('\nTest 2 ' + model_list_names[i])
            measurement = "rank"
            
            label_predict = model_list[i].predict(in_data)
            misclassified_index = np.where(label_predict==0)[0]
            misclassified = np.take(in_data, misclassified_index, axis=0)

            train_label_predict = model_list[i].predict(X)
            train_mal_index = np.where(np.logical_and(Y==1, train_label_predict==1))[0]
            train_mal = np.take(X, train_mal_index,axis=0)

            shm = shared_memory.SharedMemory(create=True, size=misclassified.nbytes)
            distances_shared = np.ndarray(misclassified.shape, dtype=misclassified.dtype, buffer=shm.buf)
            divs = misclassified.shape[0]/mp.cpu_count()

            processes = [mp.Process(target=closest_node, args=(misclassified[int(x*divs):int((x+1)*divs-1)], train_mal, shm.name, misclassified.shape, misclassified.dtype, int(x*divs), mutable_index, measurement)) for x in range(mp.cpu_count())]
            for p in processes:
                p.start()

            for p in processes:
                p.join()

            distances = np.copy(distances_shared)

            shm.close()
            shm.unlink()

            if(measurement == "average"):
                average_distance =  np.mean(distances, axis=0)
            elif (measurement == "rank"):
                average_distance = np.sum(distances, axis=0)
        
            for i in range(len(feature_labels)):
                if i in mutable_index:
                    dist = average_distance[i]
                    print(feature_labels[i] + " " + str(dist))

    # Feature dropout vulnerability scores
    if args.test == 0 or args.test == 3:

        model_retrain = [ DecisionTreeClassifier(criterion='entropy',min_samples_split=4,min_samples_leaf=2,max_depth=20,min_impurity_decrease=0.1,random_state=0),  \
        RandomForestClassifier(n_estimators=100, n_jobs=-1, criterion='entropy',max_features='auto', random_state=0), \
        LogisticRegression(solver='liblinear', random_state=0), \
        LinearDiscriminantAnalysis(solver='lsqr', shrinkage='auto'), QuadraticDiscriminantAnalysis(reg_param=0.7),\
        MLPClassifier(activation='relu', max_iter=1000, random_state=0)]

        for i in range(len(model_list)):
            logging.info('\nTest 3 ' + model_list_names[i])

            zeros = np.zeros((len(X_test),len(mutable_index)))
            X_test_dropout=np.append(X_test,zeros, axis=1)

            zeros = np.zeros((len(X),len(mutable_index)))
            X_zeroed=np.append(X,zeros, axis=1)

            X_dropout = dropout(X_zeroed, X, feature_labels, mutable_index)
            dropout_model = model_retrain[i].fit(X_dropout, Y)
            model_acc = dropout_model.score(X_test_dropout, Y_test)

            print("\nAccuracy scores:")
            test_dropout(dropout_model, X_test_dropout, Y_test, model_acc, feature_labels, len(X[0]), mutable_index)

            zeros = np.zeros((len(in_data),len(mutable_index)))
            X_mal_dropout = np.append(in_data,zeros, axis=1)
            model_acc = dropout_model.score(X_mal_dropout, labels)
            print("\nRobustness scores:")
            test_dropout(dropout_model, X_mal_dropout, labels, model_acc, feature_labels, len(X[0]), mutable_index)

    # Adversarially trained model feature importance vulnerability scores 
    if args.test == 0 or args.test == 4:

        model_retrain = [ DecisionTreeClassifier(criterion='entropy',min_samples_split=4,min_samples_leaf=2,max_depth=20,min_impurity_decrease=0.1,random_state=0),  \
        RandomForestClassifier(n_estimators=100, n_jobs=-1, criterion='entropy',max_features='auto', random_state=0), \
        LogisticRegression(solver='liblinear', random_state=0), \
        LinearDiscriminantAnalysis(solver='lsqr', shrinkage='auto'), QuadraticDiscriminantAnalysis(reg_param=0.7), \
        MLPClassifier(activation='relu', max_iter=1000, random_state=0)]

        X_train_adv, X_test_adv, y_train_adv, y_test_adv = train_test_split(in_data, labels, test_size=0.33, random_state=42)

        agg_train_X = np.concatenate((X_train_adv, X), axis=0)
        agg_train_Y = np.concatenate((y_train_adv, Y), axis=0)

        for i in range(len(model_list)):
            print(model_list_names[i])
            model_retrain[i].fit(agg_train_X, agg_train_Y)
            scikit_learn_perm(model_retrain[i], agg_train_X, agg_train_Y, feature_labels, mutable_index)

    logging.info('Done')

if __name__ == "__main__":
    main(sys.argv[1:])








