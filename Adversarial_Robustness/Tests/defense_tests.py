import os
import time
import sys
import argparse
import numpy as np
from numpy import ndarray
import pandas as pd
import multiprocessing as mp
from multiprocessing import shared_memory
from sklearn.tree import DecisionTreeClassifier
from sklearn import metrics, svm
from sklearn.naive_bayes import GaussianNB, ComplementNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report, accuracy_score, confusion_matrix
from sklearn.ensemble import VotingClassifier, RandomForestClassifier, GradientBoostingClassifier, BaggingClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
from sklearn.linear_model import LogisticRegression
import logging
from sklearn.model_selection import train_test_split
from numpy import genfromtxt
from sklearn.inspection import permutation_importance
from numpy import genfromtxt
import joblib
import csv
import random

def make_predictions_VC(model1, model2, model1_x, model2_x, y):
    pred1 = model1.predict_proba(model1_x)
    pred2 = model2.predict_proba(model2_x)
    predictions = np.argmax(np.add(pred1, pred2),1)
    return predictions

def get_outliers(in_data, inv_cov, sample_mean, outlier_alarm_signal):
    x_mu = np.subtract(in_data, sample_mean)
    left = np.dot(x_mu, inv_cov)
    mahal = np.diagonal((np.dot(left, np.transpose(x_mu))))
    outliers = mahal >= outlier_alarm_signal
    return outliers

def make_predictions_outliers(model, in_data, outliers):
    probs = model.predict_proba(in_data)
    predictions = []
    for i in range(len(in_data)):
        prob = probs[i]
        pred = np.argmax(prob)
        if outliers[i]:
            if prob[0] < .75:
                pred = 1
        predictions.append(pred)
    return predictions


def main(argv):

    parser = argparse.ArgumentParser(description='script to run output vectors against multiple classifiers')
    parser.add_argument('-v', '--verbose', default=5, type=int, help='debug message level [1-5]')
    parser.add_argument('-t', '--test', default=0, type=int, help='test method [0-4]')
    args = parser.parse_args()
    
    if args.verbose == 1:
        level=logging.CRITICAL
    elif args.verbose == 2:
        level=logging.ERROR
    elif args.verbose == 3:
        level=logging.WARNING
    elif args.verbose == 4:
        level=logging.INFO
    else:
        level=logging.DEBUG
    logging.basicConfig(level=level, format='%(asctime)s - %(levelname)s -%(message)s')

    cols = 122
    in_data_GA = np.full(cols,0.0)
    in_data_GAN = np.full(cols,0.0)
    in_data_PSO = np.full(cols,0.0)
    in_data_FE = np.full(cols,0.0)

    #GAN
    file_name_GAN = "gan_results/gan_output_vectors_NSL.csv"
    data_GAN = genfromtxt(file_name_GAN, dtype=float, delimiter=',')
    in_data_GAN = np.vstack([in_data_GAN,data_GAN])

    #GA
    start_idx_GA = 222780
    file_name_GA = "ga_results/ga_vec_output"
    #PSO
    start_idx_PSO = 2254521
    file_name_PSO = "pso_results/pso_vec_output"

    #FE
    start_idx_FE = 16973
    file_name_FE = "fe_results/fe_vec_output"

    num_files = 64
    num_files_fe=8
    for x in range(num_files):
        idx = start_idx_GA + x
        f = file_name_GA + '_' + str(idx) + '.csv'
        data_GA = genfromtxt(f, dtype=float, delimiter=',')
        in_data_GA = np.vstack([in_data_GA,data_GA])

    for x in range(num_files):
        idx = start_idx_PSO + x
        f = file_name_PSO + '_' + str(idx) + '.csv'
        data_PSO = genfromtxt(f, dtype=float, delimiter=',')
        in_data_PSO = np.vstack([in_data_PSO,data_PSO])

    for x in range(num_files_fe):
        idx = start_idx_FE + x
        f = file_name_FE + '_' + str(idx) + '.csv'
        data_FE = genfromtxt(f, dtype=float, delimiter=',')
        in_data_FE = np.vstack([in_data_FE,data_FE])

    in_data_GA = np.delete(in_data_GA,0,0)
    in_data_GAN = np.delete(in_data_GAN,0,0)
    in_data_PSO = np.delete(in_data_PSO,0,0)
    in_data_FE = np.delete(in_data_FE,0,0)

    GA_labels = np.full(len(in_data_GA),1)
    PSO_labels = np.full(len(in_data_PSO),1)
    GAN_labels = np.full(len(in_data_GAN),1)
    FE_labels = np.full(len(in_data_FE),1)

    feature_labels  = next(csv.reader(open('NSL-KDD/mod_train+_pd.csv')))
    X = genfromtxt('NSL-KDD/mod_train+_pd.csv', delimiter=',', skip_header=1)
    Y = np.load('NSL-KDD/mod_train+_labels.npy')
    X_test = genfromtxt('NSL-KDD/mod_test+_pd.csv', delimiter=',', skip_header=1)
    Y_test = np.load('NSL-KDD/mod_test+_labels.npy')

    #load saved models to test against
    #svm_file_name = 'Models/svm_model.sav'
    dt_file_name = 'Models/dt_model.sav'
    #nb_file_name = 'Models/nb_model.sav'
    #knn_file_name = 'Models/knn_model.sav'
    rf_file_name = 'Models/rf_model.sav'
    mlp_file_name = 'Models/mlp_model.sav'
    #gb_file_name = 'Models/gb_model.sav'
    lr_file_name = 'Models/lr_model.sav'
    lda_file_name = 'Models/lda_model.sav' 
    qda_file_name = 'Models/qda_model.sav' 
    #bag_file_name = 'Models/bag_model.sav'

    #svm_model = joblib.load(svm_file_name)
    dt_model = joblib.load(dt_file_name)
    #nb_model = joblib.load(nb_file_name)
    #knn_model = joblib.load(knn_file_name)
    rf_model = joblib.load(rf_file_name)
    mlp_model = joblib.load(mlp_file_name)
    #gb_model = joblib.load(gb_file_name)
    lr_model = joblib.load(lr_file_name)
    lda_model = joblib.load(lda_file_name)
    qda_model = joblib.load(qda_file_name)
    #bag_model = joblib.load(bag_file_name)

    model_list = [dt_model, rf_model, lr_model, lda_model, qda_model, mlp_model]

    model_list_names = ['DT','RF','LR','LDA','QDA', 'MLP']

    mutable_index = [0,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,101,102,103,104,109,110,111,112,113,114,115,116,117]

    #Adversarial training 

    if args.test == 0 or args.test == 1:

        print("Test 1 - Adversarial Training")
     
        model_retrain_GAN = [ DecisionTreeClassifier(criterion='entropy',min_samples_split=4,min_samples_leaf=2,max_depth=20,min_impurity_decrease=0.1,random_state=0),  \
        RandomForestClassifier(n_estimators=100, n_jobs=-1, criterion='entropy',max_features='auto', random_state=0), \
        LogisticRegression(solver='liblinear', random_state=0), \
        LinearDiscriminantAnalysis(solver='lsqr', shrinkage='auto'), QuadraticDiscriminantAnalysis(reg_param=0.7), \
        MLPClassifier(activation='relu', max_iter=1000, random_state=0)]

        model_retrain_PSO = [ DecisionTreeClassifier(criterion='entropy',min_samples_split=4,min_samples_leaf=2,max_depth=20,min_impurity_decrease=0.1,random_state=0),  \
        RandomForestClassifier(n_estimators=100, n_jobs=-1, criterion='entropy',max_features='auto', random_state=0), \
        LogisticRegression(solver='liblinear', random_state=0), \
        LinearDiscriminantAnalysis(solver='lsqr', shrinkage='auto'), QuadraticDiscriminantAnalysis(reg_param=0.7), \
        MLPClassifier(activation='relu', max_iter=1000, random_state=0)]

        model_retrain_GA = [ DecisionTreeClassifier(criterion='entropy',min_samples_split=4,min_samples_leaf=2,max_depth=20,min_impurity_decrease=0.1,random_state=0),  \
        RandomForestClassifier(n_estimators=100, n_jobs=-1, criterion='entropy',max_features='auto', random_state=0), \
        LogisticRegression(solver='liblinear', random_state=0), \
        LinearDiscriminantAnalysis(solver='lsqr', shrinkage='auto'), QuadraticDiscriminantAnalysis(reg_param=0.7),\
        MLPClassifier(activation='relu', max_iter=1000, random_state=0)]

        model_retrain_FE = [ DecisionTreeClassifier(criterion='entropy',min_samples_split=4,min_samples_leaf=2,max_depth=20,min_impurity_decrease=0.1,random_state=0),  \
        RandomForestClassifier(n_estimators=100, n_jobs=-1, criterion='entropy',max_features='auto', random_state=0), \
        LogisticRegression(solver='liblinear', random_state=0), \
        LinearDiscriminantAnalysis(solver='lsqr', shrinkage='auto'), QuadraticDiscriminantAnalysis(reg_param=0.7), \
        MLPClassifier(activation='relu', max_iter=1000, random_state=0)]

        X_train_adv_GA, X_test_adv_GA, y_train_adv_GA, y_test_adv_GA = train_test_split(in_data_GA, GA_labels, test_size=0.33, random_state=42)
        X_train_adv_GAN, X_test_adv_GAN, y_train_adv_GAN, y_test_adv_GAN = train_test_split(in_data_GAN, GAN_labels, test_size=0.33, random_state=42)
        X_train_adv_PSO, X_test_adv_PSO, y_train_adv_PSO, y_test_adv_PSO = train_test_split(in_data_PSO, PSO_labels, test_size=0.33, random_state=42)
        X_train_adv_FE, X_test_adv_FE, y_train_adv_FE, y_test_adv_FE = train_test_split(in_data_FE, FE_labels, test_size=0.33, random_state=42)
      
        agg_train_X_GA = np.concatenate((X_train_adv_GA, X), axis=0)
        agg_train_Y_GA = np.concatenate((y_train_adv_GA, Y), axis=0)
        agg_train_X_GAN = np.concatenate((X_train_adv_GAN, X), axis=0)
        agg_train_Y_GAN = np.concatenate((y_train_adv_GAN, Y), axis=0)
        agg_train_X_PSO = np.concatenate((X_train_adv_PSO, X), axis=0)
        agg_train_Y_PSO = np.concatenate((y_train_adv_PSO, Y), axis=0)
        agg_train_X_FE = np.concatenate((X_train_adv_FE, X), axis=0)
        agg_train_Y_FE = np.concatenate((y_train_adv_FE, Y), axis=0)

        for i in range(len(model_list)):
            logging.info('\nTest 1 ' + model_list_names[i])
            print("original adv accuracy GA: " +str(model_list[i].score(X_test_adv_GA, y_test_adv_GA)))
            model_adv_GA = model_retrain_GA[i].fit(agg_train_X_GA, agg_train_Y_GA)
            print("with adv training GA: " + str(model_adv_GA.score(X_test_adv_GA, y_test_adv_GA)))
            
            print("original adv accuracy PSO: " +str(model_list[i].score(X_test_adv_PSO, y_test_adv_PSO)))
            model_adv_PSO = model_retrain_PSO[i].fit(agg_train_X_PSO, agg_train_Y_PSO)
            print("with adv training PSO: " + str(model_adv_PSO.score(X_test_adv_PSO, y_test_adv_PSO)))
            
            print("original adv accuracy GAN: " +str(model_list[i].score(X_test_adv_GAN, y_test_adv_GAN)))
            model_adv_GAN = model_retrain_GAN[i].fit(agg_train_X_GAN, agg_train_Y_GAN)
            print("with adv training GAN: " + str(model_adv_GAN.score(X_test_adv_GAN, y_test_adv_GAN)))

            print("original adv accuracy FE: " +str(model_list[i].score(X_test_adv_FE, y_test_adv_FE)))
            model_adv_FE = model_retrain_FE[i].fit(agg_train_X_FE, agg_train_Y_FE)
            print("with adv training FE: " + str(model_adv_FE.score(X_test_adv_FE, y_test_adv_FE)))

            print("with GA adv training, PSO samples: " + str(model_adv_GA.score(X_test_adv_PSO, y_test_adv_PSO)))
            print("with GA adv training, GAN samples: " + str(model_adv_GA.score(X_test_adv_GAN, y_test_adv_GAN)))
            print("with GA adv training, FE samples: " + str(model_adv_GA.score(X_test_adv_FE, y_test_adv_FE)))

            print("with PSO adv training, GA samples: " + str(model_adv_PSO.score(X_test_adv_GA, y_test_adv_GA)))
            print("with PSO adv training, GAN samples: " + str(model_adv_PSO.score(X_test_adv_GAN, y_test_adv_GAN)))
            print("with PSO adv training, FE samples: " + str(model_adv_PSO.score(X_test_adv_FE, y_test_adv_FE)))

            print("with GAN adv training, PSO samples: " + str(model_adv_GAN.score(X_test_adv_PSO, y_test_adv_PSO)))
            print("with GAN adv training, GA samples: " + str(model_adv_GAN.score(X_test_adv_GA, y_test_adv_GA)))
            print("with GAN adv training, FE samples: " + str(model_adv_GAN.score(X_test_adv_FE, y_test_adv_FE)))


            print("with FE adv training, GA samples: " + str(model_adv_FE.score(X_test_adv_GA, y_test_adv_GA)))
            print("with FE adv training, PSO samples: " + str(model_adv_FE.score(X_test_adv_PSO, y_test_adv_PSO)))
            print("with FE adv training, GAN samples: " + str(model_adv_FE.score(X_test_adv_GAN, y_test_adv_GAN)))

            print("new acc on regular dataset GA: " + str(model_adv_GA.score(X_test, Y_test)))
            print("new acc on regular dataset PSO: " + str(model_adv_PSO.score(X_test, Y_test)))
            print("new acc on regular dataset GAN: " + str(model_adv_GAN.score(X_test, Y_test)))
            print("new acc on regular dataset FE: " + str(model_adv_FE.score(X_test, Y_test)))
            print("orig acc on regular dataset: " + str(model_list[i].score(X_test, Y_test)))



    #Remove mutable features 

    if args.test == 0 or args.test == 2:

        print("Test 2 - Remove Mutable Features")

        model_retrain = [ DecisionTreeClassifier(criterion='entropy',min_samples_split=4,min_samples_leaf=2,max_depth=20,min_impurity_decrease=0.1,random_state=0),  \
        RandomForestClassifier(n_estimators=100, n_jobs=-1, criterion='entropy',max_features='auto', random_state=0), \
        LogisticRegression(solver='liblinear', random_state=0), \
        LinearDiscriminantAnalysis(solver='lsqr', shrinkage='auto'), QuadraticDiscriminantAnalysis(reg_param=0.7), \
        MLPClassifier(activation='relu', max_iter=1000, random_state=0)]
        
        X_immutable = np.delete(X, mutable_index, 1)
        X_test_immutable = np.delete(X_test, mutable_index, 1)

        for i in range(len(model_list)):
            logging.info('\nTest 2 ' + model_list_names[i])
            print("orig acc: " + str(model_list[i].score(X_test, Y_test)))
            model_immutable = model_retrain[i].fit(X_immutable, Y)
            print("immutable acc: " + str(model_immutable.score(X_test_immutable, Y_test)))


    # Soft Voting Classifer 

    if args.test == 0 or args.test == 3:

        print("Test 3 - Soft Voting Classifier")

        model_retrain = [ DecisionTreeClassifier(criterion='entropy',min_samples_split=4,min_samples_leaf=2,max_depth=20,min_impurity_decrease=0.1,random_state=0),  \
        RandomForestClassifier(n_estimators=100, n_jobs=-1, criterion='entropy',max_features='auto', random_state=0), \
        LogisticRegression(solver='liblinear', random_state=0), \
        LinearDiscriminantAnalysis(solver='lsqr', shrinkage='auto'), QuadraticDiscriminantAnalysis(reg_param=0.7), \
        MLPClassifier(activation='relu', max_iter=1000, random_state=0)]

        X_immutable = np.delete(X, mutable_index, 1)
        X_test_immutable = np.delete(X_test, mutable_index, 1)

        in_data_GA_immutable = np.delete(in_data_GA, mutable_index, 1)
        in_data_PSO_immutable = np.delete(in_data_PSO, mutable_index, 1)
        in_data_GAN_immutable = np.delete(in_data_GAN, mutable_index, 1)
        in_data_FE_immutable = np.delete(in_data_FE, mutable_index, 1)

        for i in range(len(model_retrain)):
            model_immutable = model_retrain[i].fit(X_immutable, Y)
            GA_results = make_predictions_VC(model_list[i], model_immutable, in_data_GA, in_data_GA_immutable, GA_labels)
            PSO_results = make_predictions_VC(model_list[i], model_immutable, in_data_PSO, in_data_PSO_immutable, PSO_labels)
            GAN_results = make_predictions_VC(model_list[i], model_immutable, in_data_GAN, in_data_GAN_immutable,  GAN_labels)
            FE_results = make_predictions_VC(model_list[i], model_immutable, in_data_FE, in_data_FE_immutable,  FE_labels)
            orig_results = make_predictions_VC(model_list[i], model_immutable, X_test, X_test_immutable, Y_test)

            print(model_list_names[i])
            print("GA - original: " + str(model_list[i].score(in_data_GA, GA_labels)) + " VC: " + str(accuracy_score(GA_results, GA_labels)))
            print("PSO - original: " + str(model_list[i].score(in_data_PSO, PSO_labels)) + " VC: " + str(accuracy_score(PSO_results, PSO_labels)))
            print("GAN - original: " + str(model_list[i].score(in_data_GAN, GAN_labels)) + " VC: " + str(accuracy_score(GAN_results, GAN_labels)))
            print("FE - original: " + str(model_list[i].score(in_data_FE, FE_labels)) + " VC: " + str(accuracy_score(FE_results, FE_labels)))
            print("original: " + str(model_list[i].score(X_test, Y_test)) + " VC: " + str(accuracy_score(orig_results, Y_test)))


    #Outlier Alarm 

    if args.test == 0 or args.test == 4:

        print("Test 4 - Outlier Alarm")

        sample_mean = np.mean(X, 0)
        inv_cov = np.linalg.pinv(np.cov(np.transpose(X)))
        x_mu = np.subtract(X_test, sample_mean)
        left = np.dot(x_mu, inv_cov)
        mahal = np.diagonal((np.dot(left, np.transpose(x_mu))))
        Q3 = np.quantile(mahal, .75)
        mahal_std = np.std(mahal)
        outlier_alarm_signal = Q3 + 1.5 * mahal_std

        GA_outliers = get_outliers(in_data_GA, inv_cov, sample_mean, outlier_alarm_signal)
        PSO_outliers = get_outliers(in_data_PSO, inv_cov, sample_mean, outlier_alarm_signal)
        GAN_outliers = get_outliers(in_data_GAN, inv_cov, sample_mean, outlier_alarm_signal)
        FE_outliers = get_outliers(in_data_FE, inv_cov, sample_mean, outlier_alarm_signal)
        orig_outliers = get_outliers(X_test, inv_cov, sample_mean, outlier_alarm_signal)

        print("GA outliers: " + str(np.count_nonzero(GA_outliers)) + "/" + str(len(GA_outliers)))
        print("PSO outliers: " + str(np.count_nonzero(PSO_outliers)) + "/" + str(len(PSO_outliers)))
        print("GAN outliers: " + str(np.count_nonzero(GAN_outliers)) + "/" + str(len(GAN_outliers)))
        print("FE outliers: " + str(np.count_nonzero(FE_outliers)) + "/" + str(len(FE_outliers)))
        print("original outliers: " + str(np.count_nonzero(orig_outliers)) + "/" + str(len(orig_outliers)))

        for i in range(len(model_list)):
            GA_results = make_predictions_outliers(model_list[i], in_data_GA, GA_outliers)
            PSO_results = make_predictions_outliers(model_list[i], in_data_PSO, PSO_outliers)
            GAN_results = make_predictions_outliers(model_list[i], in_data_GAN, GAN_outliers)
            FE_results = make_predictions_outliers(model_list[i], in_data_FE, FE_outliers)     
            orig_results = make_predictions_outliers(model_list[i], X_test, orig_outliers)

            print(model_list_names[i])
            print("GA - original: " + str(model_list[i].score(in_data_GA, GA_labels)) + " Outlier: " + str(accuracy_score(GA_results,GA_labels)))
            print("PSO - original: " + str(model_list[i].score(in_data_PSO, PSO_labels)) + " Outlier: " + str(accuracy_score(PSO_results,PSO_labels)))
            print("GAN - original: " + str(model_list[i].score(in_data_GAN, GAN_labels)) + " Outlier: " + str(accuracy_score(GAN_results,GAN_labels)))
            print("FE - original: " + str(model_list[i].score(in_data_FE, FE_labels)) + " Outlier: " + str(accuracy_score(FE_results,FE_labels)))
            print("original: " + str(model_list[i].score(X_test, Y_test)) + " Outlier: " + str(accuracy_score(orig_results, Y_test)))



    logging.info('Done')

if __name__ == "__main__":
    main(sys.argv[1:])








