import numpy as np
import logging
from random import randint, random
import joblib
from sklearn import metrics
from sklearn.metrics import classification_report, accuracy_score
from sklearn.ensemble import VotingClassifier
import multiprocessing as mp
import copy


#  structure of this code borrows from PySwarms
#  Miranda L.J., (2018). PySwarms: a research toolkit for Particle Swarm Optimization in Python. 
#  Journal of Open Source Software, 3(21), 433, https://doi.org/joss.00433

class PartSwarm:

    mute_mask = [0,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,101,102,103,104,109,110,111,112,113,114,115,116,117]						#values of input array that are mutable
    binary_mask = [87,92,94,95,101,102]		#indices of values that are binary
    rate_mask = [110,115,117]				#indices of values who are connected to other rate values
	
    dimensions = 122						#size of input array
    
    #algorithm coefficients
    c1 = 0.5								#contribution of particle best		
    c2 = 0.4								#contribution of global best
    w = 0.7									#interia parameter
    improve_min = 0.00001					#minimum level of improvement for early termination
	
	
    def __init__(self, n_particles, dimensions, init_pos):
        ''' Initialization method for class instance '''
		
        self.n_particles = n_particles
        self.dimensions = dimensions
        self.swarm_size = (n_particles, dimensions)
        self.init_pos = copy.deepcopy(init_pos)
        self.part_best_fitness = np.full(self.n_particles, 0.0)	#create initial best fitness array
        self.part_best_location = np.full((n_particles,dimensions),0.0)  #initialize best location array
        self.fitnesses = np.empty(shape=n_particles)
        self.swarm_pos = np.empty(shape=[n_particles, 122])
        self.swarm_vel = np.empty(shape=[n_particles, 122])
        self.global_best_fitness = 0.0	
        self.global_best_part = np.empty(shape=[122,])
	
		
    def calc_fitness(self, x, model):
	
        ''' calculate particle fitness using proviced ML model '''
		
        part = copy.deepcopy(self.swarm_pos[x,:])
        part = part.reshape(1,-1)
        if x == 0:
            logging.debug('calc fitness. part: %s', part)
        #label_predict = model.predict(part)
        try:
            probas = model.predict_proba(part)
            logging.debug('probas %d is %.4f',x,probas[0,0])
            self.fitnesses[x] = copy.deepcopy(probas[0,0])
        except ValueError:
            logging.error('ValueError in predict_proba for %d',x)
            self.fitnesses[x] = 0.0    #set to 0.0 to force past it and still work.  May be less than optimal soln
        	   
		#check best particle fitness
        if self.fitnesses[x] > self.part_best_fitness[x]:
            self.part_best_fitness[x] = copy.deepcopy(self.fitnesses[x])
            self.part_best_location[x,:] = copy.deepcopy(part[0,:])
        logging.debug('self probas %d is %.4f',x,self.fitnesses[x])
           	
	
    def create_swarm(self):  #n_particles, dimensions, init_pos):
	    
        ''' Create initial particle swarm using initial position and randomized values '''
         
        particles = np.full((self.n_particles,122), 0.0)
        first_part  = copy.deepcopy(self.init_pos)
        particles = first_part  #assign one particle to be the original values
        logging.debug('self init_pos: %s',self.init_pos)
        logging.debug('create_swarm init_pos shape: %s, dtype: %s', self.init_pos.shape, self.init_pos.dtype)
        for x in range(self.n_particles-1):
            temp_part = copy.deepcopy(self.init_pos)
            for y in self.mute_mask:
                if y in self.binary_mask:
                    temp_part[y] = 1.0 if np.random.uniform() > 0.5 else 0.0
                elif y in self.rate_mask:
                    temp_part[y-1] = round(np.random.uniform(low=self.init_pos[y-1],high=1.0),10)
                    temp_part[y] = round((1.0 - temp_part[y-1]),10)
                else:
                    temp_part[y] = round((np.random.uniform(low=self.init_pos[y],high=1.0)),10)
            if x == 0:
                logging.debug('temp_part %d: \n %s', x, temp_part)
            particles = np.vstack([particles,temp_part])	
	    
        #velocities 
        self.swarm_vel = np.random.random_sample(size=(self.n_particles, self.dimensions)) 
        logging.debug('create_swarm method')
        logging.debug(self.swarm_vel)
        self.swarm_pos = copy.deepcopy(particles)
        logging.debug('create_swarm method parts')
        logging.debug(self.swarm_pos)
        
	
    def optimize(self, iters):
       
        ''' method to run the swarm algorithm '''
        self.create_swarm()
        loop_count = 0
        last_improve = 1.0
        improved = False
        
        file_name = 'Models/vc_model.sav'
        #file_name = 'Models/mlp_model.sav'
        vc = joblib.load(file_name)
        
        logging.debug('optimize method')
        logging.debug(self.swarm_pos)
        
        while ((loop_count<iters) and (last_improve >= self.improve_min)):
            last_improve = 1.0
            init_global_best_fitness = self.global_best_fitness
            
            for x in range(self.n_particles):
                self.calc_fitness(x, vc)
                
            #capture original particle fitness for comparison later
            if loop_count == 0:
                orig_fitness = self.fitnesses[0]
                logging.debug('orig_fitness is %.4f',orig_fitness)
            
            #check global best fitness and update iteration improvement
            for x in range(self.n_particles):
                if self.part_best_fitness[x] > self.global_best_fitness:
                    improved = True
                    self.global_best_fitness = copy.deepcopy(self.part_best_fitness[x])
                    self.global_best_part = copy.deepcopy(self.part_best_location[x,:])
                if improved == True:
                    last_improve = self.global_best_fitness - init_global_best_fitness
            
            logging.debug('part_best_loc shape: %s, type: %s',self.part_best_location.shape,self.part_best_location.dtype)
            logging.debug('swarm_pos shape: %s, type: %s',self.swarm_pos.shape,self.swarm_pos.dtype)
            
            #loop through particle and update velocity and position
            for y in range(self.n_particles):
                logging.debug('part_best_loc value: %s',self.part_best_location[y])
                logging.debug('swarm_pos value: %s',self.swarm_pos[y])
                for v in self.mute_mask:
                    logging.debug('y: %s, v: %s',y,v)
                    logging.debug('part_best_loc value: %s',self.part_best_location[y])
                    logging.debug('swarm_pos value: %f',self.part_best_fitness[y])
                    local_diff = self.part_best_location[y,v] - self.swarm_pos[y,v]
                    global_diff = self.global_best_part[v] - self.swarm_pos[y,v]
                    self.swarm_vel[y,v] = self.w * self.swarm_vel[y,v] + self.c1*local_diff*np.random.uniform(low=self.init_pos[v],high=1.0) + self.c2*global_diff*np.random.uniform(low=self.init_pos[v],high=1.0)
                    
                    if v in self.binary_mask:
                        self.swarm_pos[y,v] = 1 if (self.swarm_pos[y,v] + self.swarm_vel[y,v]) > 0.5 else 0
                    elif v in self.rate_mask:
                        self.swarm_pos[y,v] = self.swarm_pos[y,v] + self.swarm_vel[y,v]
                        if self.swarm_pos[y,v] > 1.0:  	#deal with exceeding the edges
                            self.swarm_pos[y,v] = 1.0
                        elif self.swarm_pos[y,v] < 0.000000001:
                            self.swarm_pos[y,v] = 0.0
                        self.swarm_pos[y,v-1] = 1.0 - self.swarm_pos[y,v]  
                    else:
                        self.swarm_pos[y,v] = self.swarm_pos[y,v] + self.swarm_vel[y,v]  
                        if self.swarm_pos[y,v] > 1.0:		#deal with exceeding the edges
                            self.swarm_pos[y,v] = 1.0
                        elif self.swarm_pos[y,v] < 0.000000001:
                            self.swarm_pos[y,v] = 0.0						
			               
            loop_count += 1
            logging.debug('Iteration %d, global best value %f',loop_count, self.global_best_fitness)

        return self.global_best_fitness, self.global_best_part, orig_fitness   
    
	
    def reset(self):
        
        ''' Initialize the swarm '''
        self.swarm_pos, self.swarm_vel = self.create_swarm()
        logging.debug('reset method')
        logging.debug(self.swarm_vel)


