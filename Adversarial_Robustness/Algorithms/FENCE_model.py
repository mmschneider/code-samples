import numpy as np
import pandas as pd
import tensorflow as tf
from numpy import genfromtxt

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Input
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.optimizers import SGD
from tensorflow.keras import backend as K

from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
K.set_learning_phase(1)

from sklearn.preprocessing import StandardScaler

def create_DNN(units, input_dim_param, lr_param):

    network = Sequential()
    network.add(Dense(units = units[0], activation = 'relu', input_dim = input_dim_param))
    network.add(Dropout(0.1))
    network.add(Dense(units = units[1], activation = 'relu'))
    network.add(Dropout(0.1))
    network.add(Dense(units = units[2], activation = 'relu'))
    network.add(Dense(units = 1, activation = 'sigmoid'))

    sgd = Adam(lr = lr_param)
    network.compile(loss = 'binary_crossentropy', optimizer = sgd)
    
    return network

X = genfromtxt('NSL-KDD/mod_train+_pd.csv', delimiter=',', skip_header=1)
Y = np.load('NSL-KDD/mod_train+_labels.npy')
X_test = genfromtxt('NSL-KDD/mod_test+_pd.csv', delimiter=',', skip_header=1)
Y_test = np.load('NSL-KDD/mod_test+_labels.npy')

y_train_array = np.array(Y)
x_train_array = np.array(X)

y_test_array = np.array(Y_test)
x_test_array = np.array(X_test)

scaler = StandardScaler(copy = True, with_mean = True, with_std = True)
x_train_array = scaler.fit_transform(x_train_array)
x_test_array = scaler.transform(x_test_array)

LAYERS = [256, 128, 64]
INPUT_DIM = 122
LR = [0.0002592943797404667]

for lrate in LR:
 
  nn =  create_DNN(units = LAYERS, input_dim_param = INPUT_DIM, lr_param = lrate)

  nn.fit(x_train_array, y_train_array, verbose = 0, epochs = 50, batch_size = 64,
                              shuffle = True)
                              
  probas = nn.predict_proba(x_test_array)
  predictions = (probas >= 0.5).astype(int)  

  print('F1', f1_score(y_test_array, predictions))
  print('accuracy', accuracy_score(y_test_array, predictions))

  nn.save('FENCE_model')