import numpy as np
import tensorflow as tf
import copy


class Fence:

    mutable_index = [0,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,101,102,103,104,109,110,111,112,113,114,115,116,117]
    binary_idxs = [3,8,10,11,16,17]
    linked_idxs_l = [20,25,27]
    linked_idxs_h = [21,26,28]

    file_name = 'FENCE_model'
    model = tf.keras.models.load_model(file_name)
    
    dimensions = 122    #size of input array
    
    #algorithm coefficients
    M=500 # Max steps
    alpha = .01 # Learning Rate
    
    def __init__(self):
        ''' Initialization method for class instance '''     
  
    def find_sample(self, pos):
        m=0
        sample = copy.deepcopy(pos)
        mutable_features = copy.deepcopy(self.mutable_index)
        while m < self.M and len(mutable_features) > 0:
            x = tf.convert_to_tensor(copy.deepcopy(pos))
            pred = self.model.predict(np.reshape(sample, (-1,122)))[0][0]
            if pred  <= .5:
                if (m == 0):
                    #sample was already misclassified 
                    return (m, pred, None)
                else:
                    return(m, pred, sample)
            with tf.GradientTape() as tape:
                tape.watch(x)
                prediction = self.model(tf.reshape(x,[-1,122]))
                loss = tf.keras.losses.MSE([[1]], prediction)
                gradient = tape.gradient(loss, x)
                gradient_array = gradient.numpy()
                gradient_mute = np.take(gradient_array, mutable_features)
                i_max_mute = np.argmax(gradient_mute)
                i_max = mutable_features[i_max_mute]
                grad = gradient_array[i_max]
            if grad <= 0:
                break
            if i_max in self.binary_idxs:
                sample[i_max] = 1 if sample[i_max] == 0 else 0
                del mutable_features[i_max_mute]
            else:
                perturbation = sample[i_max] + self.alpha*grad
                if perturbation >= 1:
                    perturbation = 1
                    del mutable_features[i_max_mute]
                if perturbation < pos[i_max]:
                    perturbation = pos[i_max]
                    del mutable_features[i_max_mute]
                sample[i_max] = perturbation
            if i_max in self.linked_idxs_l:
                sample[i_max+1] = 1-perturbation
            elif i_max in self.linked_idxs_h:
                sample[i_max-1] = 1-perturbation
            m += 1
        return (m, pred, None)

