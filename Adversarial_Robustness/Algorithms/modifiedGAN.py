# Import relevant libraries
import os
import time
import sys
import argparse
import numpy as np
from numpy import ndarray
import pandas as pd
from pandas.api.types import CategoricalDtype
import logging
import copy
import tensorflow.keras
from tensorflow.keras import layers
from tensorflow.keras import models
from tensorflow.keras.models import load_model, Model
from sklearn.ensemble import VotingClassifier
import joblib
import warnings
from tensorflow.keras import backend as K
import csv
from sklearn import svm
from sklearn.naive_bayes import GaussianNB, ComplementNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from numpy import genfromtxt



def recall_m(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())
    return recall

def precision_m(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision

def f1_m(y_true, y_pred):
    precision = precision_m(y_true, y_pred)
    recall = recall_m(y_true, y_pred)
    return 2*((precision*recall)/(precision+recall+K.epsilon()))


 # Load data from files and prepare for algorithm use
def read_data(input_file):
    
    #add names to dataset to make working with columns easier!
    col_names = ["duration","proto","service","flag","src_bytes","dst_bytes","land","wrong_frag", 
    "urgent","hot","num_failed_logins","logged_in","num_comp","root_shell","su_att","num_root","num_file_cr", 
    "num_shells","num_access_files","num_outbound_cmds","is_hot_login","is_guest_login","count", 
    "srv_count","serror_rate","srv_serror_rate","rerror_rate","srv_rerror_rate","same_srv_rate", 
    "diff_srv_rate","srv_diff_host_rate","dst_host_count","dst_host_srv_count","dst_host_same_srv", 
    "dst_host_diff_srv","dst_host_same_src_port","dst_host_srv_diff_host","dst_host_serror", 
    "dst_host_srv_serror","dst_host_rerror","dst_host_srv_rerror","label","difficulty"]

    logging.info('reading input file')
    data = pd.read_csv(input_file, names = col_names)
    logging.info('input data loaded')
    logging.debug(data.shape)
    logging.debug((data != 0).any(axis=0))
    labels = data.pop('label').values
    difficulty = data.pop('difficulty').values  
    
    return data, labels, difficulty

# Check to ensure that lists of values in categorical data are equivalent in size
# and then create union of the sets
def equalize_categories(test_df, train_df):
    ts_proto_list = test_df['proto'].unique()
    tr_proto_list = train_df['proto'].unique()
    ts_srv_list = test_df['service'].unique()
    tr_srv_list = train_df['service'].unique()
    ts_flag_list = test_df['flag'].unique()
    tr_flag_list = train_df['flag'].unique()
    proto_list = list(set().union(ts_proto_list,tr_proto_list))
    srv_list = list(set().union(ts_srv_list,tr_srv_list))
    flag_list = list(set().union(ts_flag_list,tr_flag_list))
    
    return proto_list, srv_list, flag_list


def mod_data(in_df, in_labels, proto_list, srv_list, flag_list, feat, drop_list):
    
    #name to drop if not using 'All' features
    nc_names = ["duration", "urgent", "num_failed_logins", "num_root", "num_access_files",
                "num_outbound_cmds", "is_hot_login", "diff_srv_rate","srv_diff_host_rate",
                "dst_host_same_src_port","dst_host_srv_diff_host"]
    #names to drop if not using 'SLC' features
    lc_names = ["src_bytes", "land", "hot", "logged_in","num_comp","root_shell",
                "su_att","num_file_cr", "num_shells", "is_guest_login", "rerror_rate", 
                "dst_host_srv_rerror"]

    drop_names = drop_list

    logging.info('engineering the dataset')

    #normalize numeric columns to values between 0 and 1
    norm_names = ["src_bytes","dst_bytes","wrong_frag", 
    "urgent","hot","num_failed_logins","num_comp","num_root","num_file_cr", 
    "num_shells","num_access_files","num_outbound_cmds","count", 
    "srv_count","dst_host_count","dst_host_srv_count"]
    
      
    #set ceiling for each norm_name column value
    ceilings = {'duration':65535,'src_bytes':4294967295,'dst_bytes':4294967295,'wrong_frag':10,'urgent':10,
                'hot':100,'num_failed_logins':10,'num_comp':10000,'num_root':10000,'num_file_cr':100,
                'num_shells':10,'num_access_files':20,'num_outbound_cmds':10,'count':1000,'srv_count':1000,
                'dst_host_count':255,'dst_host_srv_count':255}
    
    result = in_df.copy()            
    
    #modify duration column to log scale and min-max scale it
    in_df['duration'] = in_df['duration'].apply(lambda x: np.log(1+x))
    max_value = ceilings.get('duration')    
    logging.debug('%s max: %d','duration', max_value)
    min_value = 0       		  #make absolute min 
    #don't modify columns that are all zeroes!!
    if (max_value-min_value) != 0:
        result['duration'] = (in_df['duration'] - min_value) / (max_value - min_value)
    
    #one-hot encode the categorical columns
    #not a final solution as all possible values could be missed
    logging.debug(result.shape)
    if feat == 0 or feat == 3:   #proto is no contribution, only modifiy if using All features
        cat_type = CategoricalDtype(categories=proto_list)
        cat = in_df['proto'].astype(cat_type)
        one_hot = pd.get_dummies(cat) 
        result.drop(['proto'],axis = 1,inplace=True)
        for i in range(len(proto_list)):
            col_name = 'proto'+ str(i)
            result.insert(loc=(i+1), column=col_name, value=one_hot.iloc[:, [i]])
        logging.debug('shape post-proto dummies: %s',result.shape)
    #else:
        #result = result.drop(columns="proto")
    
    cat_type = CategoricalDtype(categories=srv_list)
    cat = in_df['service'].astype(cat_type)
    one_hot = pd.get_dummies(cat) 
    result.drop(['service'],axis = 1,inplace=True)
    for i in range(len(srv_list)):
        col_name = 'service'+ str(i)
        result.insert(loc=(i+4), column=col_name, value=one_hot.iloc[:, [i]])
    logging.debug('shape post-service dummies: %s',result.shape)
    
    cat_type = CategoricalDtype(categories=flag_list)
    cat = in_df['flag'].astype(cat_type)
    one_hot = pd.get_dummies(cat)  
    result.drop(['flag'],axis = 1,inplace=True)
    for i in range(len(flag_list)):
        col_name = 'flag'+ str(i)
        result.insert(loc=(i+74), column=col_name, value=one_hot.iloc[:, [i]])
    logging.debug('shape post-flag dummies: %s',result.shape)

    # normalize all other column values
    for feature_name in norm_names:
        max_value = ceilings.get(feature_name)    
        logging.debug('%s max: %d',feature_name, max_value)
        min_value = 0       					  #make absolute min
        #don't modify columns that are all zeroes!!
        if (max_value-min_value) != 0:
            result[feature_name] = (in_df[feature_name] - min_value) / (max_value - min_value)
    logging.debug('shape post-norming: %s',result.shape)

    #make results all floating point type for uniformity
    result = result.astype(float)
    logging.debug(result.dtypes)
    logging.debug(result.head(2))
    
    #modify features used based on command line input
    if feat == 1 or feat == 2:   #using SLC feature set
        #for c in nc_names:
        result = result.drop(columns=nc_names)
    if feat == 2:  #using SC feature set
        #for c in lc_names:
        result = result.drop(columns=lc_names)
    if feat == 3:
        result = result.drop(columns=drop_names)
    logging.info('final engineered dataset shape is: %s',result.shape)

    
    #convert labels to binary values, normal = 0, attack = 1
    out_labels = ndarray((in_df['proto'].count()),int)
    x = 0
    for lbl in in_labels:
        if lbl == 'normal':
            out_labels[x] = 0
        else:
            out_labels[x] = 1
        x += 1
    logging.debug('output labels:')
    logging.debug(out_labels)
    return result, out_labels

def split_vecs(vec_array, fixed_indices, mutable_indices):
    fixed_vecs = np.full(len(fixed_indices),0.0)
    mute_vecs = np.full(len(mutable_indices),0.0)

    for vec in vec_array:
        fixed = np.array(vec)
        fixed = np.take(fixed, fixed_indices)		

        mute = np.array(vec)
        mute = np.take(mute, mutable_indices)

        fixed_vecs = np.vstack([fixed_vecs,fixed])
        mute_vecs = np.vstack([mute_vecs,mute])
        
    fixed_vecs = np.delete(fixed_vecs,0,0)
    mute_vecs = np.delete(mute_vecs,0,0) 
        
    return fixed_vecs, mute_vecs

def add_noise(in_vecs, binary_mask, rate_h_mask, rate_l_mask, perturb_mask, mutable_len):
## Add noise to the malicious traffic
## in_vecs is shape(29) - only the muteable parts of the vector

    train_mal_noise_mute_df = np.full(mutable_len, 0.0)
    MAX_PERTURB = min(20, mutable_len)
    MIN_PERTURB = 5

    num_perturb = np.random.randint(MIN_PERTURB,MAX_PERTURB)  #pick number of features to perturb randomly
    perturb_list = np.random.choice(perturb_mask, num_perturb, replace=False) #pick features to perturb
    # Add noise to existing values in the vector - done inline
    # randomly add noise to mutable features
    for vec in in_vecs:
        temp_vec = copy.deepcopy(vec)
        for y in perturb_list:
            if y in binary_mask:
                temp_vec[y] = 1 if temp_vec[y] == 0 else 0
            elif y in rate_h_mask:
                temp_vec[y] = round(np.random.uniform(low=temp_vec[y],high=1.0),10)
                temp_vec[y+1] = round((1.0 - temp_vec[y]),10)
            else:
                temp_vec[y] = np.random.uniform(low=temp_vec[y],high=1.0)
        train_mal_noise_mute_df = np.vstack([train_mal_noise_mute_df,temp_vec])
    
    train_mal_noise_mute_df = np.delete(train_mal_noise_mute_df,0,0)
    
    return train_mal_noise_mute_df  
    
# function to ensure the features have valid values (e.g., binary, rates, float)

def vec_adjust(vec_array, mute_mask, binary_mask, rate_mask, col_len):
    
    out_vec = np.empty([col_len])
    for vec in vec_array:
        temp_vec = copy.deepcopy(vec)
        for y in mute_mask:
            if y in binary_mask:
                temp_vec[y] = 1.0 if vec[y] > 0.5 else 0.0
            elif y in rate_mask:
                if vec[y-1] < 0:
                    temp_vec[y-1] = 0.0
                elif vec[y-1] > 1.0:
                    temp_vec[y-1] = 1.0
                temp_vec[y] = 1.0 - temp_vec[y-1]
            else:
                if vec[y] < 0:
                    temp_vec[y] = 0.0
                elif vec[y] > 1.0:
                    temp_vec[y] = 1.0
        out_vec = np.vstack([out_vec,temp_vec])
    out_vec = np.delete(out_vec,0,0)
    
    return out_vec
  
def conv_feature(vec_array_mute, vec_array_fixed, col_len, order):
	    
    out_array = np.full(col_len,0.0)
    if (len(vec_array_mute) != len(vec_array_fixed)):
        log.ERROR("Vectors not equal")

    for x,y in zip(vec_array_mute,vec_array_fixed):  
        feature = []
        mute_index = 0
        fixed_index = 0
        feature = []
        for elem in order:
            if elem == 0:
                feature.append(x[mute_index])
                mute_index+=1
            else:
                feature.append(y[fixed_index])
                fixed_index+=1
        out_array = np.vstack([out_array,feature])       
    out_array = np.delete(out_array,0,0)
    
    return copy.deepcopy(out_array)  
    
def sel_rand_rows(in_array, dim, rows):
    
    #randomly sample batch of rows from input array
    sample_mat = np.empty(shape=[0,dim])
    
    for i in rows:
        sample = []
        sample = in_array[i,:]
        sample_mat = np.vstack([sample_mat, sample]) 
        
    sample_mat = np.delete(sample_mat,0,0)
    
    return sample_mat
    
   
def main(argv):
    parser = argparse.ArgumentParser(description='GAN implementation using NSL-KDD dataset')
    parser.add_argument('-v', '--verbose', default=5, type=int, help='debug message level [1-5]')
    parser.add_argument('-r', '--remove', type=int, nargs='+', help='feature indices to remove. Order highest to lowest', required=True)
    args = parser.parse_args()
    
    if args.verbose == 1:
        level=logging.CRITICAL
    elif args.verbose == 2:
        level=logging.ERROR
    elif args.verbose == 3:
        level=logging.WARNING
    elif args.verbose == 4:
        level=logging.INFO
    else:
        level=logging.DEBUG
    logging.basicConfig(level=level, format='%(asctime)s - %(levelname)s -%(message)s')

    drop_list_ind = args.remove
    feature_labels  = next(csv.reader(open('NSL-KDD/mod_train+_pd.csv')))
    drop_list = list(np.take(feature_labels, drop_list_ind))

    print("removing " + str(drop_list) + " ")

    binary_mask = [3,8,10,11,16,17]   #indices of values that are binary
    rate_h_mask = [20,25,27]   #indices of values who are connected to other rate values
    rate_l_mask = [21,26,28]
    rate_mask = [110,115,117]   
    binary_mask_adj = [87,92,94,95,101,102]
    mute_mask = [0,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,101,102,103,104,109,110,111,112,113,114,115,116,117] 
    num_mutable = 29 - len(drop_list)
    new_dim = 122-len(drop_list)
    perturb_mask = list(range(0, num_mutable-1))

    for i in range(len(drop_list_ind)):
        removed = 0
        for j in range(len(binary_mask)):
            if binary_mask[j-removed] == drop_list_ind[i]:
                del binary_mask[j-removed]
                removed += 1
            elif binary_mask[j-removed-removed] > drop_list_ind[i]:
                binary_mask[j-removed] = binary_mask[j-removed] - 1
        removed = 0
        for j in range(len(rate_h_mask)):
            if rate_l_mask[j-removed] == drop_list_ind[i]:
                del rate_h_mask[j-removed]
                del rate_l_mask[j-removed]
                removed += 2
            elif rate_l_mask[j-removed] > drop_list_ind[i]:
                rate_l_mask[j-removed] = rate_l_mask[j-removed] - 1
                rate_h_mask[j-removed] = rate_h_mask[j-removed] - 1
        removed = 0                   
        for j in range(len(rate_mask)):
            if rate_mask[j-removed] == drop_list_ind[i]:
                del rate_mask[j-removed]
                removed += 1
            elif rate_l_mask[j-removed] > drop_list_ind[i]:
                rate_mask[j-removed] = rate_mask[j-removed] - 1
        removed = 0
        for j in range(len(binary_mask_adj)):
            if binary_mask_adj[j-removed] == drop_list_ind[i]:
                del binary_mask_adj[j-removed]
                removed += 1
            elif binary_mask_adj[j-removed] > drop_list_ind[i]:
                binary_mask_adj[j-removed] = binary_mask_adj[j-removed] - 1
        removed = 0
        for j in range(len(mute_mask)):
            if mute_mask[j-removed] == drop_list_ind[i]:
                del mute_mask[j-removed]
                removed += 1
            elif mute_mask[j-removed] > drop_list_ind[i]:
                mute_mask[j-removed] = mute_mask[j-removed] - 1

    fixed_indices = list(range(0, new_dim))
    fixed_indices = np.delete(fixed_indices, mute_mask)
    fixed_indices = list(fixed_indices)

    order = []
    for i in range(0,new_dim):
        if i in mute_mask:
            order.append(0)
        else:
            order.append(1)
    print(order)

    # Divide the training data set into the normal traffic and the malicious traffic

    train_df, train_lbls, train_diff = read_data('NSL-KDD/KDDTrain+.txt')
    test_df, test_lbls, test_diff = read_data('NSL-KDD/KDDTest+.txt')
    proto_list, srv_list, flag_list = equalize_categories(test_df, train_df)
    train_df, train_lbls = mod_data(train_df, train_lbls, proto_list, srv_list, flag_list, 3, drop_list)
    train_df["label"] = train_lbls.tolist()
    training_df = train_df.copy()
    training_df.sort_values("label", inplace = True)
    train_norm_df = training_df.loc[training_df['label'] == 0]
    train_mal_df = training_df.loc[training_df['label'] == 1]

    test_df, test_lbls = mod_data(test_df, test_lbls, proto_list, srv_list, flag_list, 3, drop_list)
    test_df["label"] = test_lbls.tolist()
    test_mal_df = test_df.loc[test_df['label'] == 1]
    test_mal_labels = test_mal_df.pop('label').values

    # Remove the "label" column from normal traffic so that only features are inputs
    labels_norm = train_norm_df.pop('label').values

    # Convert the normal traffic and test traffic from pandas df to numpy
    train_norm_df = train_norm_df.to_numpy()
    test_mal_df = test_mal_df.to_numpy()
    
     #break malicious vectors into mutable and non-mutable portions

    # Remove the "label" column from malicious traffic so that only features are inputs
    labels_mal = train_mal_df.pop('label').values

    # Convert the malicious traffic from pandas df to numpy
    train_mal_df = train_mal_df.to_numpy()
    
    fixed_mal_vecs, mute_mal_vecs = split_vecs(train_mal_df, fixed_indices, mute_mask)
    fixed_norm_vecs, mute_norm_vecs = split_vecs(train_norm_df, fixed_indices, mute_mask)

    
    generator = models.Sequential() # instantiates the generator model
    generator.add(layers.Dense(new_dim, activation='relu', input_dim=new_dim))
    generator.add(layers.Dense(num_mutable, activation='relu'))
    generator.add(layers.Dense(num_mutable, activation='relu'))
    generator.add(layers.Dense(num_mutable, activation='relu'))
    generator.add(layers.Dense(num_mutable, activation='relu'))
    generator.add(layers.Dense(num_mutable, activation='linear'))
    generator.summary()

    generator_optimizer = tensorflow.keras.optimizers.SGD(lr=0.01, clipvalue=0.01)

    generator.compile(optimizer=generator_optimizer, loss='mean_squared_error')
    
    ## Train the DISCRIMINATOR

    # Neural network (3 linear layers with ReLU and sigmoid activation)

    discriminator = models.Sequential() #instantiates the discriminator model
    discriminator.add(layers.Dense(new_dim, activation='relu', input_dim=new_dim))
    discriminator.add(layers.Dense(new_dim, activation='relu'))
    discriminator.add(layers.Dense(1, activation='sigmoid'))
    discriminator.summary()

    discriminator_optimizer = tensorflow.keras.optimizers.RMSprop(lr=0.01, clipvalue=0.01)

    discriminator.compile(optimizer=discriminator_optimizer, loss='binary_crossentropy', metrics=['binary_accuracy'])
    
    X = genfromtxt('NSL-KDD/mod_train+_pd.csv', delimiter=',', skip_header=1)
    Y = np.load('NSL-KDD/mod_train+_labels.npy')
    X = np.delete(X, drop_list_ind, 1)

    svm_model = svm.SVC(gamma=0.01, C=220.0, tol=0.01,probability=True)
    svm_model.fit(X, Y)
    dt_model = DecisionTreeClassifier(criterion='entropy',min_samples_split=4,min_samples_leaf=2,max_depth=20,min_impurity_decrease=0.1,random_state=0)
    dt_model.fit(X, Y)
    nb_model= ComplementNB()
    nb_model.fit(X, Y)
    knn_model = KNeighborsClassifier(n_neighbors=3, algorithm='auto')
    knn_model.fit(X, Y)

    vc = VotingClassifier(estimators=[('svm', svm_model), ('dt', dt_model), ('nb', nb_model), ('knn', knn_model)], voting='soft',n_jobs= -1)
    vc = vc.fit(X, Y)
    #vc = joblib.load('Models/NSL_KDD_Models/vc_model_4weighted_soft.sav')
    warnings.filterwarnings("ignore")
    joblib.dump(vc, "vc_modified")
    logging.info("Voting Classifier Trained")
    
    iterations = 300
    num_steps = 10
    batch_size = 50

    d_loss_store = []
    a_loss_store = []

    for loop_step in range(iterations):
        for G_Steps in range(num_steps):   #loop that trains generator on malicious traffic
            randomRows = np.random.randint(low=0,high=len(fixed_mal_vecs), size=batch_size)  #get random rows from dataset
            mute_norm_traffic = sel_rand_rows(mute_norm_vecs,num_mutable,randomRows) 
            fixed_mal = sel_rand_rows(fixed_mal_vecs,93,randomRows)
            mute_mal = sel_rand_rows(mute_mal_vecs, num_mutable,randomRows)
            noise_mal = add_noise(mute_mal, binary_mask, rate_h_mask, rate_l_mask, perturb_mask, num_mutable)
            train_mal_noise_df = conv_feature(noise_mal,fixed_mal, new_dim, order)
            generated_traffic = generator.predict_on_batch(train_mal_noise_df)  #generate adversarial examples using generator
            generated_traffic_full = conv_feature(generated_traffic,fixed_mal, new_dim, order)  #reassemble vector for processing
            generated_traffic_full = vec_adjust(generated_traffic_full, mute_mask, binary_mask_adj, rate_mask, new_dim)         #fix values to have appropriate values (i.e. valid)
            gen_traffic_full_fixed, gen_traff_full_mute = split_vecs(generated_traffic_full, fixed_indices, mute_mask)
            discrim_labels = discriminator.predict(generated_traffic_full)
            discrim_fdbk = np.empty([num_mutable])
            for idx, label in enumerate(discrim_labels):
                if label == '0':    #thinks it's normal
                    temp_fdbk = gen_traff_full_mute[idx,:] 
                else:
                    temp_fdbk = mute_norm_traffic[idx,:]   #thinks it's malicious, feed it back something that's normal
                discrim_fdbk = np.vstack([discrim_fdbk,temp_fdbk])
            discrim_fdbk = np.delete(discrim_fdbk,0,0)     #delete extra initial entry
            a_loss = generator.train_on_batch(train_mal_noise_df, discrim_fdbk)
            a_loss_store.append(a_loss) #store the adversarial loss
            
        for D_Steps in range(num_steps):    #loop that trains generator on good traffic
            randomRows = np.random.randint(low=0,high=len(mute_norm_vecs), size=batch_size)  #get random rows from dataset
            norm_traffic = sel_rand_rows(train_norm_df,new_dim,randomRows)
            combined_traffic = np.concatenate((generated_traffic_full, norm_traffic),axis=0)
            discrim_labels = discriminator.predict_classes(combined_traffic)
            vc_labels = vc.predict(combined_traffic)
            # calc loss for feedback to training
            discrim_fdbk = np.empty([1])
            for x,y in zip(discrim_labels,vc_labels):
                if (x == 1 and y == 1):    #agree on label mal
                      temp_fdbk = 1
                elif (x == 0 and y == 0):  #agree on label norm
                      temp_fdbk = 0
                elif (x == 1 and y == 0):  #passes bb
                      temp_fdbk = 0
                else:                      #doesn't pass bb
                      temp_fdbk = 0
                discrim_fdbk = np.vstack([discrim_fdbk,temp_fdbk])
            discrim_fdbk = np.delete(discrim_fdbk,0,0)        
            d_loss = discriminator.train_on_batch (combined_traffic, discrim_fdbk) 
            d_loss_store.append(d_loss) #store the disriminator loss
            
        if loop_step % 10 == 0: #print metrics and save weights every 100 steps
            logging.info('discriminator %s = %s; %s = %s', discriminator.metrics_names[0],d_loss[0], discriminator.metrics_names[1], d_loss[1])
            logging.info('adversarial loss: %s', a_loss) 
    
    discriminator.save('Models/NSL-KDD_discriminator_model.h5')        
    test_mal_fixed_vecs, test_mal_mute_vecs = split_vecs(test_mal_df, fixed_indices, mute_mask)
    generated_traffic = generator.predict(test_mal_df)
    final_traffic = conv_feature(generated_traffic,test_mal_fixed_vecs, new_dim, order)
    final_traffic = vec_adjust(final_traffic, mute_mask, binary_mask_adj, rate_mask, new_dim)
    post_score = vc.score(final_traffic, test_mal_labels)
    logging.info('Model Accuracy: %f',post_score)
    np.savetxt("modified_gan_results/gan_output_vectors_NSL.csv", final_traffic, delimiter=",")
    
        

if __name__ == "__main__":
    main(sys.argv[1:])
