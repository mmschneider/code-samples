## code based off code found at:  https://github.com/100/Solid/blob/master/Solid/GeneticAlgorithm.py

import os
import sys
from abc import ABCMeta, abstractmethod
from copy import deepcopy
from random import randint, random, shuffle
import numpy as np
import joblib
from sklearn.tree import DecisionTreeClassifier
from sklearn import metrics, svm
from sklearn.naive_bayes import GaussianNB, ComplementNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report, accuracy_score
from sklearn.ensemble import VotingClassifier
import logging
import copy


class GeneticAlgorithm:
    """
    Conducts genetic algorithm
    """
    __metaclass__ = ABCMeta

    population = None
    fitnesses = None
    max_chromosomes = 100

    crossover_rate = None
    mutation_rate = None

    cur_steps = 0
    best_fitness = 0.0
    best_member = None

    max_steps = 1000
    max_fitness = 1.0
    mut_start_idx = 93  # location in chromosome array where the mutable section begins
    seed_floors = None  # initial floor values for mutable portion of the input data

    fixed = None
    

    def __init__(self, crossover_rate, mutation_rate, max_steps, max_fitness=None):
        """

        :param crossover_rate: probability of crossover
        :param mutation_rate: probability of mutation
        :param max_steps: maximum steps to run genetic algorithm for
        :param max_fitness: fitness value to stop algorithm once reached
        """
        if isinstance(crossover_rate, float):
            if 0 <= crossover_rate <= 1:
                self.crossover_rate = crossover_rate
            else:
                raise ValueError('Crossover rate must be a float between 0 and 1')
        else:
            raise ValueError('Crossover rate must be a float between 0 and 1')

        if isinstance(mutation_rate, float):
            if 0 <= mutation_rate <= 1:
                self.mutation_rate = mutation_rate
            else:
                raise ValueError('Mutation rate must be a float between 0 and 1')
        else:
            raise ValueError('Mutation rate must be a float between 0 and 1')

        if isinstance(max_steps, int) and max_steps > 0:
            self.max_steps = max_steps
        else:
            raise ValueError('Maximum steps must be aimport copy positive integer')

        if max_fitness is not None:
            if isinstance(max_fitness, (int, float)):
                self.max_fitness = float(max_fitness)
            else:
                raise ValueError('Maximum fitness must be a numeric type')

    def __str__(self):
        return ('GENETIC ALGORITHM: \n' +
                'CURRENT STEPS: %d \n' +
                'BEST FITNESS: %f \n' +
                'BEST MEMBER: %s\n\n') % \
               (self.cur_steps, self.best_fitness, str(self.best_member))

    def __repr__(self):
        return self.__str__()

    def _clear(self):
        """
        Resets the variables that are altered on a per-run basis of the algorithm

        :return: None
        """
        self.cur_steps = 0
        self.population = np.full((self.max_chromosomes,122), 0.0)
        self.fitnesses = np.full(122, 0.0)   
        self.best_member = np.full(29, 0.0)
        self.best_fitness = 0.0
        self.seed_floors = np.full(29, 0.0)
        self.fixed = np.full(93, 0.0)

    def _cap_fixed(self, in_array):
	
	#routine to capture fixed portion (immutable) of input array for the algorithm
	

        fixed = copy.deepcopy(in_array[1:85])		#protocol, service, flag
        fixed = np.concatenate((fixed,in_array[100]), axis=None)    #num_outbound_cmds
        fixed = np.concatenate((fixed,in_array[105:109]), axis=None) #server error rates
        fixed = np.concatenate((fixed,in_array[118:]), axis=None)  #dst error rates
        
        return fixed


    def _conv_feature(self, chromo):
	
	#routine to convert from chromosome format to feature format
	

        feature = chromo[0]
        feature = np.concatenate((feature,self.fixed[0:84]), axis=None)
        feature = np.concatenate((feature,chromo[1:16]), axis=None)
        feature = np.concatenate((feature,self.fixed[84]), axis=None)
        feature = np.concatenate((feature,chromo[16:20]), axis=None)
        feature = np.concatenate((feature,self.fixed[85:89]), axis=None)
        feature = np.concatenate((feature,chromo[20:]), axis=None)
        feature = np.concatenate((feature,self.fixed[89:]), axis=None)
        
        return copy.deepcopy(feature)

    def _conv_mute(self, chromo):
        
        #routine to convert full feature vector to mutable chromosome
        
        
        mute = chromo[0]					#create chromosome based on mutable portion of the seed
        mute = np.concatenate((mute,chromo[85:100]), axis=None)	#	#duration + src_bytes thru num_access_files 
        mute = np.concatenate((mute,chromo[101:105]), axis=None)	##is_hot_login thru srv_count
        mute = np.concatenate((mute,chromo[109:118]), axis=None)  	##same_srv_rate thru dst_host_srv_diff_host_rate

        return copy.deepcopy(mute)

    #@abstractmethod
    def _initial_population(self, seed):
        """
        Generates initial population -
        members must be represented by a list (array)
        seed is a list of mixed integer and floating point numbers
        :return: list of members of population

        Generate N chromosomes(100).  
        """

        self.fixed = copy.deepcopy(self._cap_fixed(seed))	#capture fixed portion of the seed
                
        mute = self._conv_mute(seed)				#capture mutable portion of the seed
        self.seed_floors = copy.deepcopy(mute)
        chromosomes = copy.deepcopy(mute)			#capture seed in the initial population
                
        for x in range(self.max_chromosomes-1):
            new_mute = np.random.uniform(low=mute[0],high=1.0)	 	##duration
            new_mute = np.append(new_mute, np.random.uniform(low=mute[1],high=1.0)) #src_bytes
            new_mute = np.append(new_mute, np.random.uniform(low=mute[2],high=1.0)) #dst_bytes
            new_mute = np.append(new_mute, (1 if np.random.uniform() > 0.5 else 0)) #land
            new_mute = np.append(new_mute, np.random.uniform(low=mute[4],high=1.0)) #wrong_fragment
            new_mute = np.append(new_mute, np.random.uniform(low=mute[5],high=1.0)) #urgent
            new_mute = np.append(new_mute, np.random.uniform(low=mute[6],high=1.0)) #hot
            new_mute = np.append(new_mute, np.random.uniform(low=mute[7],high=1.0)) #num_failed_logins
            new_mute = np.append(new_mute,(1 if np.random.uniform() > 0.5 else 0))  #logged_in
            new_mute = np.append(new_mute,np.random.uniform(low=mute[9],high=1.0))  #num_comprom
            new_mute = np.append(new_mute,(1 if np.random.uniform() > 0.5 else 0))  #root_shell
            new_mute = np.append(new_mute,(1 if np.random.uniform() > 0.5 else 0))  #su_attempted
            new_mute = np.append(new_mute,np.random.uniform(low=mute[12],high=1.0))  #num_root
            new_mute = np.append(new_mute,np.random.uniform(low=mute[13],high=1.0))  #num_file_creations
            new_mute = np.append(new_mute,np.random.uniform(low=mute[14],high=1.0))  #num_shells
            new_mute = np.append(new_mute,np.random.uniform(low=mute[15],high=1.0))  #num_access_files
            new_mute = np.append(new_mute,(1 if np.random.uniform() > 0.5 else 0))  # is_hot_login
            new_mute = np.append(new_mute,(1 if np.random.uniform() > 0.5 else 0))  # is_guest_login
            new_mute = np.append(new_mute,np.random.uniform(low=mute[18],high=1.0))  #count
            new_mute = np.append(new_mute,np.random.uniform(low=mute[19],high=1.0)) #srv_count
            new_mute = np.append(new_mute,np.random.uniform(low=mute[20],high=1.0))  #same_srv_rate
            new_mute = np.append(new_mute,(1-new_mute[20]))  #diff_srv_rate
            new_mute = np.append(new_mute,np.random.uniform(low=mute[22],high=1.0)) #srv_diff_host_rate
            new_mute = np.append(new_mute,np.random.uniform(low=mute[23],high=1.0))  #dst_host_count
            new_mute = np.append(new_mute,np.random.uniform(low=mute[24],high=1.0))  #dst_host_srv_count
            new_mute = np.append(new_mute,np.random.uniform(low=mute[25],high=1.0))  #dst_host_same_srv_rate
            new_mute = np.append(new_mute,(1-new_mute[25]))  #dst_host_diff_srv_rate
            new_mute = np.append(new_mute,np.random.uniform(low=mute[27],high=1.0))  #dst_host_same_src_port_rate
            new_mute = np.append(new_mute,(1-new_mute[27]))  #dst_host_diff_src_port_rate
            chromosomes= np.vstack([chromosomes,copy.deepcopy(new_mute)])
	    
        return chromosomes

    
    def _fitness(self, member, model):
        """
        Evaluates fitness of a given member

        :param member: a member
        :return: fitness of member

        Use %age that chromosome is in benign category from ML Ensemble
        create ML ensemble using 4 algorithms developed currently
        """
        
        logging.debug('member shape %s',member.shape)      
        # convert chromosome to format required by classifier
        #features = self._conv_feature(member)
        # reshape array so it is acceptable by the predictor
        features = member.reshape(1,-1)
        logging.debug('features shape %s', features.shape)
        #label_predict = model.predict(features)
        probas = model.predict_proba(features)

        # return the probability of being classified as 'normal' aka not malicious
        return probas[0,0]
		
    def _populate_fitness(self, model):
        """
        Calculates fitness of all members of current population

        :return: None
        """
        self.fitnesses = np.full(np.size(self.population,0),0.0)    #clear old fitnesses
        count = 0 
        for x in self.population:
            # convert chromosome to format required by classifier
            logging.debug('x shape %s', x.shape)
            features = self._conv_feature(x)
            logging.debug('features shape %s', features.shape)
            # reshape array so it is acceptable by the predictor
            features = features.reshape(1,-1)
            #label_predict = model.predict(features)
            probas = model.predict_proba(features)
            #self.fitnesses.append(probas[0,0])
            self.fitnesses[count] = copy.deepcopy(probas[0,0])
            count += 1
        
		 
    def _most_fit(self):
        """
        Finds most fit member of current population

        :return: most fit member and most fit member's fitness

        Keep this chromosome in next population
        """
        best_idx = 0
        for x in range(np.size(self.population,0)):  
            if self.fitnesses[x] > self.fitnesses[best_idx]:
                best_idx = x 
       
        return copy.deepcopy(self.population[best_idx,:]), self.fitnesses[best_idx]

    def _select_n(self, n):
        """
        Probabilistically selects n members from current population using
        roulette-wheel selection

        :param n: number of members to select
        :return: n members
        """
        #shuffle(self.population)  no real need to do this!
        # let's sort by fitness though!!
        #print("length population is %d, fitnesses length %d", len(self.population), len(self.fitnesses))
        sorted_fitness = np.argsort(self.fitnesses)
        total_fitness = sum(self.fitnesses)
        if total_fitness != 0:
            probs = list([self.fitnesses[sorted_fitness[x]] / total_fitness for x in range(len(self.population))]) #self.population]) #changed from _fitness
        else:
            return self.population[0:n]
        res = np.full(122,0.0)
        for z in range(n):
            r = random()
            sum_ = 0.0
            for i, x in enumerate(probs):
                sum_ += x  #probs[i]
                if r <= sum_:
                    if z == 0:
                        res = deepcopy(self.population[sorted_fitness[i],:])
                    else:
                        res = np.vstack([res,deepcopy(self.population[sorted_fitness[i],:])])
                    break
        return res

    def _select_cross(self):
        """
        Probabilistically selects 2 members from current population 
        :return: 2 members
        """
        res = np.full((2,122),0.0)
        r = np.random.randint(low= 0, high= self.max_chromosomes-1) 
        s = np.random.randint(low=0, high= self.max_chromosomes-1) 
        
        while (s == r):				# ensure we pick 2 different parents!!
            s= np.random.randint(low=0,high= self.max_chromosomes-1) 
        logging.debug('r %s, s %s',r, s)
        logging.debug('r shape %s',self.population[r].shape)
        logging.debug('s shape %s', self.population[s].shape)
        res = copy.deepcopy(self.population[r,:])
        res = np.vstack([res,copy.deepcopy(self.population[s,:])])
        logging.debug('res shape %s', res.shape)  
        return res


    def _crossover(self, parent1, parent2):
        """
        Creates new member of population by combining two parent members

        :param parent1: a member
        :param parent2: a member
        :return: member made by combining elements of both parents
        """
        partition = randint(1, 28)	#pick from possible columns in mutable portion
        linked_idxs_h = [21,26,28]
        if partition in linked_idxs_h:
            partition -= 1		#adjust to ensure related field is in the cut
        #print('partition', partition)
        #parent1.reshape(parent1.size,1)
        
        child1 = np.concatenate((parent1[0:partition],parent2[partition:]), axis=None)
        child2 = np.concatenate((parent2[0:partition],parent1[partition:]), axis=None)
        logging.debug('child1 shape: %s, child 2 shape: %s',child1.shape,child2.shape)
        return child1, child2

    def _mutate(self, chromosome):
        """
        Randomly mutates a member

        :param member: a member
        :return: mutated member
        """
        binary_idxs = [3,8,10,11,16,17]
        linked_idxs_l = [20,25,27]
        linked_idxs_h = [21,26,28]
        if random() <= self.mutation_rate:
            member = copy.deepcopy(chromosome)
            #print(member)
            idx = randint(0, 28)
            #print('index ',idx)
            if idx in binary_idxs:
                member[idx] = 1 if member[idx] == 0 else 0
            elif idx in linked_idxs_l:
                # can change to any value >= original seed value
                member[idx] = np.random.uniform(low=self.seed_floors[idx],high=1.0)
                member[idx+1] = 1 - member[idx]
            elif idx in linked_idxs_h:
                member[idx] = np.random.uniform(low=self.seed_floors[idx],high=1.0)
                member[idx-1] = 1 - member[idx]
            else:
                value = np.random.uniform(low=self.seed_floors[idx],high=1.0)
                #print('value ',value)
                member[idx] = value
            self.population = np.vstack([self.population,copy.deepcopy(member)])
       

    def run(self, seed):
        """
        Conducts genetic algorithm

        :param verbose: indicates whether or not to print progress regularly
        :return: best state and best objective function value
        """
        self._clear()
        self.population = self._initial_population(seed)
        file_name = 'Models/vc_model.sav'
        vc = joblib.load(file_name)
        orig_fitness = self._fitness(seed, vc)
        self.best_fitness = orig_fitness
        self.best_member = self._conv_mute(seed)  #copy.deepcopy(seed)
        last_improve = 1.0
        num_copy = max(int((1 - self.crossover_rate) * self.max_chromosomes), 2)
        num_crossover = self.max_chromosomes - num_copy
        no_improve_count = 0
        for i in range(self.max_steps):
            self.cur_steps += 1
            
            temp_pop = np.full(122,0.0)
            for z in range(num_crossover):
                parents = self._select_cross()  
                temp_chromo1, temp_chromo2 = self._crossover(parents[0,:],parents[1,:])
                logging.debug('temp chromo1 shape %s, chromo2 %s ', temp_chromo1.shape,temp_chromo2.shape)
                if z == 0:
                    temp_pop = copy.deepcopy(temp_chromo1)
                else:
                    temp_pop = np.vstack([temp_pop, copy.deepcopy(temp_chromo1)])
                temp_pop = np.vstack([temp_pop, copy.deepcopy(temp_chromo2)])

            self.population = np.vstack([self.population, temp_pop])

            for x in self.population: 
                self._mutate(x)  
            
            logging.debug('pop shape post mutate %s', self.population.shape)
            
            self._populate_fitness(vc)

            best_member, best_fitness = self._most_fit()
            last_improve = best_fitness - self.best_fitness
            logging.debug("iteration %d best fitness is %.4f",i, best_fitness)
            if best_fitness > self.best_fitness:
                self.best_fitness = best_fitness
                self.best_member = deepcopy(best_member)
                no_improve_count = 0
            else:
                no_improve_count += 1
            
            # select next generation of chromosomes from current population pool while keeping the best member
            self.population = self._select_n(self.max_chromosomes-1)
            logging.debug('population shape %s', self.population.shape)
            logging.debug('best_member shape %s',self.best_member.shape)
            self.population = np.vstack([self.population, self.best_member])	
                        
            if self.max_fitness is not None and self.best_fitness >= self.max_fitness:
                print("TERMINATING - REACHED MAXIMUM FITNESS")
                return self._conv_feature(self.best_member), self.best_fitness, orig_fitness
            if last_improve < 0.0001 and no_improve_count > 5:
                print("TERMINATING - NO IMPROVEMENT after " + i + " steps")
                return self._conv_feature(self.best_member), self.best_fitness, orig_fitness
        print("TERMINATING - REACHED MAXIMUM STEPS")
        return self._conv_feature(self.best_member), self.best_fitness, orig_fitness

