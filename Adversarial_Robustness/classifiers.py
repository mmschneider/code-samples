import os
import time
import sys
import argparse
import numpy as np
from numpy import ndarray
import pandas as pd
from pandas.api.types import CategoricalDtype
from sklearn import preprocessing
#depricated 
#from sklearn.externals import joblib
import joblib
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn import metrics, svm
from sklearn.naive_bayes import GaussianNB, ComplementNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report, accuracy_score, confusion_matrix
from sklearn.ensemble import VotingClassifier, RandomForestClassifier, GradientBoostingClassifier, BaggingClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
from sklearn.linear_model import LogisticRegression
import logging
from ga import GeneticAlgorithm
from MySwarms import PartSwarm
from MySwarms_NB import PartSwarm_NB
from ga_NB import GeneticAlgorithm_NB
from FENCE import Fence
import multiprocessing as mp
import copy

##  load data from files and prepare for algorithm use
def read_data(input_file, dataset):
    
    logging.info('reading input file')
    #add names to dataset to make working with columns easier!
    if dataset == 0:	#NSL-KDD set
        col_names = ["duration","proto","service","flag","src_bytes","dst_bytes","land","wrong_frag", 
    "urgent","hot","num_failed_logins","logged_in","num_comp","root_shell","su_att","num_root","num_file_cr", 
    "num_shells","num_access_files","num_outbound_cmds","is_hot_login","is_guest_login","count", 
    "srv_count","serror_rate","srv_serror_rate","rerror_rate","srv_rerror_rate","same_srv_rate", 
    "diff_srv_rate","srv_diff_host_rate","dst_host_count","dst_host_srv_count","dst_host_same_srv", 
    "dst_host_diff_srv","dst_host_same_src_port","dst_host_srv_diff_host","dst_host_serror", 
    "dst_host_srv_serror","dst_host_rerror","dst_host_srv_rerror","label","difficulty"]
        data = pd.read_csv(input_file, names = col_names)
    elif dataset == 1:   #UNSW_NB15 set
        col_names = ['id','dur','proto','service','state','spkts','dpkts','sbytes','dbytes','rate','sttl','dttl',
        'sload','dload','sloss','dloss','sinpkt','dinpkt','sjit','djit','swin','stcpb','dtcpb','dwin','tcprtt',
	'synack','ackdat','smean','dmean','trans_depth','response_body_len','ct_srv_src','ct_state_ttl','ct_dst_ltm',
	'ct_src_dport_ltm','ct_dst_sport_ltm','ct_dst_src_ltm','is_ftp_login','ct_ftp_cmd','ct_flw_http_mthd',
	'ct_src_ltm','ct_srv_dst','is_sm_ips_ports','attack_cat','label']
        col_types={'id':str,'dur':object,'proto':str,'service':str,'state':str,'spkts':str,'dpkts':str,'sbytes':str,'dbytes':str,
'rate':str,'sttl':str,'dttl':str,'sload':str,'dload':str,'sloss':str,'dloss':str,'sinpkt':str,'dinpkt':str,'sjit':str,
'djit':str,'swin':str,'stcpb':str,'dtcpb':str,'dwin':str,'tcprtt':str,'synack':str,'ackdat':str,'smean':str,'dmean':str,
'trans_depth':str,'response_body_len':str,'ct_srv_src':str,'ct_state_ttl':str,'ct_dst_ltm':str,'ct_src_dport_ltm':str,
'ct_dst_sport_ltm':str,'ct_dst_src_ltm':str,'is_ftp_login':str,'ct_ftp_cmd':str,'ct_flw_http_mthd':str,'ct_src_ltm':str,'ct_srv_dst':str,
'is_sm_ips_ports':str,'attack_cat':str,'label':str}

        data = pd.read_csv(input_file)  #, names = col_names, dtype=col_types)
    
    logging.info('input data loaded')
    logging.debug(data.shape)
    logging.debug((data != 0).any(axis=0))
    labels = data.pop('label').values
    if dataset == 0:
        difficulty = data.pop('difficulty').values  
    elif dataset == 1:
        atttack_cat = data.pop('attack_cat').values
        data["service"]= data["service"].replace("-", "none") 	#fix empty service value in this feature column
    
    return data, labels

##  check to ensure that lists of values in categorical data are equivalent in size
##  and then create union of the sets
def equalize_categories(test_df, train_df, dataset):
    ts_proto_list = test_df['proto'].unique()
    tr_proto_list = train_df['proto'].unique()
    ts_srv_list = test_df['service'].unique()
    tr_srv_list = train_df['service'].unique()
    proto_list = list(set().union(ts_proto_list,tr_proto_list))
    srv_list = list(set().union(ts_srv_list,tr_srv_list))
    if dataset == 0:
        ts_flag_list = test_df['flag'].unique()
        tr_flag_list = train_df['flag'].unique()
    else:
        ts_flag_list = test_df['state'].unique()
        tr_flag_list = train_df['state'].unique()

    flag_list = list(set().union(ts_flag_list,tr_flag_list))

    return proto_list, srv_list, flag_list
    
    
#engineer the dataset and labels
def mod_data(in_df, in_labels, proto_list, srv_list, flag_list, feat):
    
    #name to drop if not using 'All' features
    nc_names = ["duration", "urgent", "num_failed_logins", "num_root", "num_access_files",
                "num_outbound_cmds", "is_hot_login", "diff_srv_rate","srv_diff_host_rate",
                "dst_host_same_src_port","dst_host_srv_diff_host"]
    #names to drop if not using 'SLC' features
    lc_names = ["src_bytes", "land", "hot", "logged_in","num_comp","root_shell",
                "su_att","num_file_cr", "num_shells", "is_guest_login", "rerror_rate", 
                "dst_host_srv_rerror"]

    drop_names = ["su_att","is_guest_login"]

    logging.info('engineering the dataset')

    #normalize numeric columns to values between 0 and 1
    norm_names = ["src_bytes","dst_bytes","wrong_frag", 
    "urgent","hot","num_failed_logins","num_comp","num_root","num_file_cr", 
    "num_shells","num_access_files","num_outbound_cmds","count", 
    "srv_count","dst_host_count","dst_host_srv_count"]
    
      
    #set ceiling for each norm_name column value
    ceilings = {'duration':65535,'src_bytes':4294967295,'dst_bytes':4294967295,'wrong_frag':10,'urgent':10,
                'hot':100,'num_failed_logins':10,'num_comp':10000,'num_root':10000,'num_file_cr':100,
                'num_shells':10,'num_access_files':20,'num_outbound_cmds':10,'count':1000,'srv_count':1000,
                'dst_host_count':255,'dst_host_srv_count':255}
    
    result = in_df.copy()            
    
    #modify duration column to log scale and min-max scale it
    in_df['duration'] = in_df['duration'].apply(lambda x: np.log(1+x))
    max_value = ceilings.get('duration')    
    logging.debug('%s max: %d','duration', max_value)
    min_value = 0       		  #make absolute min 
    #don't modify columns that are all zeroes!!
    if (max_value-min_value) != 0:
        result['duration'] = (in_df['duration'] - min_value) / (max_value - min_value)
    
    #one-hot encode the categorical columns
    #not a final solution as all possible values could be missed
    logging.debug(result.shape)
    if feat == 0 or feat == 3:   #proto is no contribution, only modifiy if using All features
        cat_type = CategoricalDtype(categories=proto_list)
        cat = in_df['proto'].astype(cat_type)
        one_hot = pd.get_dummies(cat) 
        result.drop(['proto'],axis = 1,inplace=True)
        for i in range(len(proto_list)):
            col_name = 'proto'+ str(i)
            result.insert(loc=(i+1), column=col_name, value=one_hot.iloc[:, [i]])
        logging.debug('shape post-proto dummies: %s',result.shape)
    else:
        result = result.drop(columns="proto")

    cat_type = CategoricalDtype(categories=srv_list)
    cat = in_df['service'].astype(cat_type)
    one_hot = pd.get_dummies(cat) 
    result.drop(['service'],axis = 1,inplace=True)
    for i in range(len(srv_list)):
        col_name = 'service'+ str(i)
        result.insert(loc=(i+4), column=col_name, value=one_hot.iloc[:, [i]])
    logging.debug('shape post-service dummies: %s',result.shape)
    
    cat_type = CategoricalDtype(categories=flag_list)
    cat = in_df['flag'].astype(cat_type)
    one_hot = pd.get_dummies(cat)  
    result.drop(['flag'],axis = 1,inplace=True)
    for i in range(len(flag_list)):
        col_name = 'flag'+ str(i)
        result.insert(loc=(i+74), column=col_name, value=one_hot.iloc[:, [i]])
    logging.debug('shape post-flag dummies: %s',result.shape)

    # normalize all other column values
    for feature_name in norm_names:
        max_value = ceilings.get(feature_name)    
        logging.debug('%s max: %d',feature_name, max_value)
        min_value = 0       					  #make absolute min
        #don't modify columns that are all zeroes!!
        if (max_value-min_value) != 0:
            result[feature_name] = (in_df[feature_name] - min_value) / (max_value - min_value)
    logging.debug('shape post-norming: %s',result.shape)

    #make results all floating point type for uniformity
    result = result.astype(float)
    logging.debug(result.dtypes)
    logging.debug(result.head(2))
    
    #modify features used based on command line input
    if feat == 1 or feat == 2:   #using SLC feature set
        #for c in nc_names:
        result = result.drop(columns=nc_names)
    if feat == 2:  #using SC feature set
        #for c in lc_names:
        result = result.drop(columns=lc_names)
    if feat == 3:
        result = result.drop(columns=drop_names)
    logging.info('final engineered dataset shape is: %s',result.shape)

    
    #convert labels to binary values, normal = 0, attack = 1
    out_labels = ndarray((in_df['proto'].count()),int)
    x = 0
    for lbl in in_labels:
        if lbl == 'normal':
            out_labels[x] = 0
        else:
            out_labels[x] = 1
        x += 1
    logging.debug('output labels:')
    logging.debug(out_labels)
    return result, out_labels


#  Function to extract only malicious vectors from test dataset
def extract_malware(in_df, in_labels, dimensions):
    file_name = 'NSL-KDD/test_malicious.npy'
    out_array = np.empty(shape=[0, dimensions])
    for x in range(len(in_labels)):
        if in_labels[x] == 1:
            out_array =  np.vstack([out_array, in_df.iloc[x]])
            if x == 0:
                logging.debug('extracted line:')
                logging.debug(out_array)
    if (not os.path.isfile(file_name)):
        np.save(file_name, out_array)
    
    return out_array


#  LinearSVC classifier.
def default_svm (test_label, test_input, train_label, train_input):
    file_name = 'Models/svm_model.sav'
    start_time=time.time()
    #SVM = svm.LinearSVC()
    #check to see if model already trained and saved
    if (not os.path.isfile(file_name)):
        SVM = svm.SVC(gamma=0.01, C=220.0, tol=0.01,probability=True)  #IgZ13 paper uses this version it seems. Doesn't work though.
        SVM.fit(train_input, train_label)
        joblib.dump(SVM, file_name)
    else:
        SVM = joblib.load(file_name)
    label_prediction = SVM.predict(test_input)
    logging.info('runtime is: %f',(time.time()-start_time))
    logging.info('score is: %f', SVM.score(test_input, test_label))
    logging.info('\n%s', classification_report(test_label,label_prediction))
    

#  Naive Bayes Classifier
def default_nb (test_label, test_input, train_label, train_input):
    file_name = 'Models/nb_model.sav'
    start_time=time.time()
    #gnb = GaussianNB()
    if (not os.path.isfile(file_name)):
        gnb = ComplementNB()  #IgZ13 paper uses this version it seems. 
        gnb.fit(train_input,train_label)
        joblib.dump(gnb, file_name)
    else:
        gnb = joblib.load(file_name)
    y_pred = gnb.predict(test_input)
    logging.info('runtime is: %f',(time.time()-start_time))
    logging.info('score is: %f', gnb.score(test_input, test_label))
    logging.info('\n%s',classification_report(test_label, y_pred))
    

#  decision tree classifier - doesn't use C4.5 algorithm, uses CART
def default_dec_tree(test_label, test_input, train_label, train_input):
    file_name = 'Models/dt_model.sav'
    start_time=time.time()
    if (not os.path.isfile(file_name)):
        dt = DecisionTreeClassifier(criterion='entropy',min_samples_split=4,min_samples_leaf=2,max_depth=20,min_impurity_decrease=0.1)
        dt.fit(train_input,train_label)
        joblib.dump(dt, file_name)
    else:
        dt = joblib.load(file_name)
    y_pred = dt.predict(test_input)
    logging.info('runtime is: %f',(time.time()-start_time))
    logging.info('score is: %f', dt.score(test_input, test_label)) 
    logging.info('\n%s',classification_report(test_label, y_pred))
    
def default_knn(test_label, test_input, train_label, train_input):
    file_name = 'Models/knn_model.sav'
    start_time=time.time()
    if (not os.path.isfile(file_name)):
        knn = KNeighborsClassifier(n_neighbors=3, algorithm='auto')
        knn.fit(train_input,train_label)
        joblib.dump(knn, file_name)
    else:
        knn = joblib.load(file_name)
    y_pred = knn.predict(test_input)
    logging.info('runtime is: %f',(time.time()-start_time))
    logging.info('score is: %f', knn.score(test_input, test_label))
    logging.info('\n%s',classification_report(test_label, y_pred))

def default_random_forest(test_label, test_input, train_label, train_input):
    file_name = 'Models/rf_model.sav'
    start_time=time.time()
    if (not os.path.isfile(file_name)):    
        RF=RandomForestClassifier(n_estimators=100, n_jobs=-1, criterion='entropy',max_features='auto', random_state=0)
        RF.fit(train_input, train_label)
        joblib.dump(RF, file_name)
    else:
        RF = joblib.load(file_name)
    y_pred = RF.predict(test_input)
    logging.info('runtime is: %f',(time.time()-start_time))
    logging.info('score is: %f', RF.score(test_input, test_label))
    logging.info('\n%s',classification_report(test_label, y_pred))

def default_MLP(test_label, test_input, train_label, train_input):
    file_name = 'Models/mlp_model.sav'
    start_time=time.time()
    if (not os.path.isfile(file_name)):
        MLP = MLPClassifier(activation='relu', max_iter=1000)
        MLP.fit(train_input, train_label)
        joblib.dump(MLP, file_name)
    else:
        MLP = joblib.load(file_name)     
    y_pred = MLP.predict(test_input)
    logging.info('runtime is: %f',(time.time()-start_time))
    logging.info('score is: %f', MLP.score(test_input, test_label))
    logging.info('\n%s',classification_report(test_label, y_pred))

def default_gb (test_label, test_input, train_label, train_input):
    file_name = 'Models/gb_model.sav'
    start_time=time.time()
    if (not os.path.isfile(file_name)):
        GB = GradientBoostingClassifier()  
        GB.fit(train_input,train_label)
        joblib.dump(GB, file_name)
    else:
        GB = joblib.load(file_name)
    y_pred = GB.predict(test_input)
    logging.info('runtime is: %f',(time.time()-start_time))
    logging.info('score is: %f',GB.score(test_input, test_label))
    logging.info('\n%s',classification_report(test_label, y_pred))

def default_lr (test_label, test_input, train_label, train_input): 
    file_name = 'Models/lr_model.sav'
    start_time=time.time()
    if (not os.path.isfile(file_name)):
        LR = LogisticRegression(solver='liblinear')  
        LR.fit(train_input,train_label)
        joblib.dump(LR, file_name)
    else:
        LR = joblib.load(file_name)
    y_pred = LR.predict(test_input)
    logging.info('runtime is: %f',(time.time()-start_time))
    logging.info('score is: %f',LR.score(test_input, test_label))
    logging.info('\n%s',classification_report(test_label, y_pred))

def default_lda(test_label, test_input, train_label, train_input):
    file_name = 'Models/lda_model.sav'
    start_time=time.time() 
    if (not os.path.isfile(file_name)):
        LDA = LinearDiscriminantAnalysis(solver='lsqr', shrinkage='auto')
        LDA.fit(train_input,train_label)
        joblib.dump(LDA, file_name)
    else:
        LDA = joblib.load(file_name)
    y_pred = LDA.predict(test_input)
    logging.info('runtime is: %f',(time.time()-start_time))
    logging.info('score is: %f', LDA.score(test_input, test_label))
    logging.info('\n%s',classification_report(test_label, y_pred))

def default_qda(test_label, test_input, train_label, train_input):
    file_name = 'Models/qda_model.sav' 
    start_time=time.time()
    if (not os.path.isfile(file_name)):
        QDA = QuadraticDiscriminantAnalysis(reg_param=0.7)
        QDA.fit(train_input,train_label)
        joblib.dump(QDA, file_name)
    else:
        QDA = joblib.load(file_name)
    y_pred = QDA.predict(test_input)
    logging.info('runtime is: %f',(time.time()-start_time))
    logging.info('score is: %f', QDA.score(test_input, test_label))
    logging.info('\n%s',classification_report(test_label, y_pred))

def default_bag(test_label, test_input, train_label, train_input):
    file_name = 'Models/bag_model.sav' 
    start_time=time.time()
    if (not os.path.isfile(file_name)):
        BAG = BaggingClassifier()
        BAG.fit(train_input,train_label)
        joblib.dump(BAG, file_name)
    else:
        BAG = joblib.load(file_name)
    y_pred = BAG.predict(test_input)
    logging.info('runtime is: %f',(time.time()-start_time))
    logging.info('score is: %f', BAG.score(test_input, test_label))
    logging.info('\n%s',classification_report(test_label, y_pred))

  
def ga_pool(mal_array, orig_max_dict, dset, dummy):
    ''' Parallel processing version of Genetic algorithm method '''
    out_file_name = "ga_results/ga_output_"+str(os.getpid())+".csv"
    out_file = open(out_file_name,'w')
    out_vec_file_name = "ga_results/ga_vec_output_"+str(os.getpid())+".csv"
    count = 1
    success = 0 
    if dset == 0:
        result_array = np.empty(shape=[0,122])
    elif dset == 1:
        result_array = np.empty(shape=[0,196])
    for test_array in mal_array:
        if dset == 0:
            my_ga = GeneticAlgorithm(0.25, 0.2, 100, max_fitness=1.0)
        elif dset == 1:
            my_ga = GeneticAlgorithm_NB(0.25, 0.2, 100, orig_max_dict, max_fitness=1.0)
        start_time=time.time()
        best_array, norm_prob, orig_fitness = my_ga.run(test_array) 
        #if (best_array[1] != 0 and best_array[1] != 1):
        #    logging.error('error in protocol field!')
        #    logging.error('input array:\n %s',test_array)
        #    logging.error('best_array:\n %s',best_array)
        #    exit(1)
        end_time = time.time()
        if count != 1:
            result_array = np.vstack([result_array,best_array])
        else:
            result_array = copy.deepcopy(best_array)
        logging.info('pid: %d, original fitness of part %d is: %.4f',os.getpid(),count, orig_fitness)
        logging.info('pid: %d, probability of normal is for vector: %.4f',os.getpid(), norm_prob)
        out_file.write('{},{},{},{}\n'.format(orig_fitness,norm_prob,(norm_prob-orig_fitness),(end_time-start_time)))
        count += 1 
        if norm_prob > 0.5:
            success += 1
    accuracy = success/count
    logging.info('pid: %d, modified accuracy is: %f',os.getpid(), accuracy)
    out_file.write('modified accuracy is: {}'.format(accuracy))
    out_file.close()
    np.savetxt(out_vec_file_name, result_array,delimiter=',', fmt='%1.6f',newline='\n')

def ps_pool(mal_array,dset,dummy, cols):
    ''' Parallel Processing version of Particle Swarm algorithm method 
        Put in dummy parameter to get it to work.  Does nothing!    '''

    out_file_name = "pso_results/pso_output_"+str(os.getpid())+".csv"
    out_file = open(out_file_name,'w')
    out_vec_file_name = "pso_results/pso_vec_output_"+str(os.getpid())+".csv"
    proc_id = os.getpid()
    count = 1
    success = 0
    if dset == 0:
        result_array = np.empty(shape=[0,cols])
    elif dset == 1:
        result_array = np.empty(shape=[0,cols])
    for test_array in mal_array:
        start_time=time.time()
        logging.debug('test_array: %s',test_array)
        if dset == 0:
            my_ps = PartSwarm(200, cols, init_pos=test_array)
        elif dset == 1:
            my_ps = PartSwarm_NB(200, cols, init_pos=test_array)
        global_best_fitness, global_best_part, orig_fitness = my_ps.optimize(100)
        result_array = np.vstack([result_array,global_best_part])
        logging.info('pid: %d, original fitness of part %d is: %.4f, probability of normal is: %.4f',proc_id,count,orig_fitness, global_best_fitness)
        logging.debug('best part pid: %d, \n %s',proc_id, global_best_part)
        end_time=time.time()
        out_file.write('{},{},{},{}\n'.format(orig_fitness,global_best_fitness,(global_best_fitness-orig_fitness),(end_time-start_time))) 
        count += 1
        if global_best_fitness > 0.5:
            success += 1
    accuracy = success/count
    logging.info('modified accuracy is: %f', accuracy)
    out_file.write('modified accuracy is: {}'.format(accuracy))
    out_file.close()
    np.savetxt(out_vec_file_name, result_array,delimiter=',', fmt='%f',newline='\n')

def fe_pool(mal_array,dset,dummy, cols):
    ''' Parallel Processing version of FENCE algorithm method 
        Put in dummy parameter to get it to work.  Does nothing!    '''

    out_vec_file_name = "fe_results/fe_vec_output_"+str(os.getpid())+".csv"
    proc_id = os.getpid()
    if dset == 0:
        result_array = []
    elif dset == 1:
        result_array = np.empty(shape=[0,cols])
    if dset == 0:
        my_fe = Fence()
    #elif dset == 1:
    # 
    for i in range(len(mal_array)):
        m, pred, sample = my_fe.find_sample(mal_array[i])
        if sample is not None:
            print(str(i) + " SAMPLE: " + str(m) + " steps and accuracy is: " + str(pred))
            result_array.append(sample) 
        else:
            print(str(i) + " NONE: " + str(m) + " steps and accuracy is: " + str(pred))

    np.savetxt(out_vec_file_name, np.array(result_array), delimiter=',', fmt='%f',newline='\n')

def mod_duration(duration):
    if 'E' in duration:
        return "0.0000001"
    else:
        return duration


#engineer the dataset and labels
def engr_data(in_df, in_labels, proto_list, srv_list, flag_list):
    
    logging.info('engineering the dataset')

    #normalize numeric columns to values between 0 and 1
    norm_names = ["spkts","dpkts","sbytes","dbytes","rate","sttl","dttl","sload","dload","sloss","dloss","sinpkt",
                  "dinpkt","sjit","djit","stcpb","dtcpb","tcprtt","synack","ackdat","smean","dmean","trans_depth","response_body_len",
                  "ct_srv_src","ct_dst_ltm","ct_src_dport_ltm","ct_dst_src_ltm","ct_src_ltm","ct_srv_dst","ct_state_ttl",
                  "ct_dst_sport_ltm"]
    
    result_df = in_df.copy()            
    result_df.drop(['id'], axis=1,inplace=True)

    #modify duration column to log scale and min-max scale it
    in_df['dur'] = in_df['dur'].astype(float)
    in_df['dur'] = in_df['dur'].apply(lambda x: np.log(1+x))
    max_value = in_df['dur'].max()   
    logging.debug('%s max: %f','duration', max_value)
    min_value = 0       		  #make absolute min 
    #don't modify columns that are all zeroes!!
    if (max_value-min_value) != 0:
        result_df['dur'] = (in_df['dur'] - min_value) / (max_value - min_value)
    
    #one-hot encode the categorical columns
    #not a final solution as all possible values could be missed
    logging.debug(result_df.shape)
    cat = in_df['proto'].astype('category',categories=proto_list)
    one_hot = pd.get_dummies(cat) 
    result_df.drop(['proto'],axis = 1,inplace=True)
    for i in range(len(proto_list)):
        col_name = 'proto'+ str(i)
        result_df.insert(loc=(i+1), column=col_name, value=one_hot.iloc[:, [i]])
    logging.debug('shape post-proto dummies: %s',result_df.shape)
        
    cat = in_df['service'].astype('category',categories=srv_list)
    one_hot = pd.get_dummies(cat) 
    result_df.drop(['service'],axis = 1,inplace=True)
    for i in range(len(srv_list)):
        col_name = 'service'+ str(i)
        result_df.insert(loc=(i+4), column=col_name, value=one_hot.iloc[:, [i]])
    logging.debug('shape post-service dummies: %s',result_df.shape)

    cat = in_df['state'].astype('category',categories=flag_list)
    one_hot = pd.get_dummies(cat)  
    result_df.drop(['state'],axis = 1,inplace=True)
    for i in range(len(flag_list)):
        col_name = 'state'+ str(i)
        result_df.insert(loc=(i+74), column=col_name, value=one_hot.iloc[:, [i]])
    logging.debug('shape post-state dummies: %s',result_df.shape)

    # normalize all numeric column values
    orig_max_dict = {}
    for feature_name in norm_names:
        max_value = in_df[feature_name].max()  
        orig_max_dict.update({feature_name:max_value})   
        logging.debug('%s max: %d',feature_name, max_value)
        min_value = in_df[feature_name].min()					
        #don't modify columns that are all zeroes!!
        if (max_value-min_value) != 0:
            result_df[feature_name] = (in_df[feature_name] - min_value) / (max_value - min_value)
    logging.debug('shape post-norming: %s',result_df.shape)

    result_df['swin_bin'] = result_df['swin'].apply(lambda x: 0 if x == 0 else 1)  #binarize the feature
    result_df.drop(['swin'],axis=1, inplace=True)
    result_df['dwin_bin'] = result_df['dwin'].apply(lambda x: 0 if x == 0 else 1)  #binarize the feature
    result_df.drop(['dwin'],axis=1, inplace=True)

    #make results all floating point type for uniformity
    result_df = result_df.astype(float)
    logging.debug(result_df.dtypes)
    logging.debug(result_df.head(2))
    
    logging.info('final engineered dataset shape is: %s',result_df.shape)
    
    #convert labels to binary values, normal = 0, attack = 1
    out_labels = ndarray((in_df['proto'].count()),int)
    x = 0
    for lbl in in_labels:
        if lbl == 0:
            out_labels[x] = 0
        else:
            out_labels[x] = 1
        x += 1
    logging.debug('output labels:')
    logging.debug(out_labels)
    return result_df, out_labels, orig_max_dict

def main(argv):
    parser = argparse.ArgumentParser(description='Classifiers for testing comparison')
    parser.add_argument('-a', '--alg', default='NA', choices=['SVM','NB','DT','KNN','VC','RF','MLP','LDA','LR','GB','QDA','BAG','NA'], help='algorithm to use')
    parser.add_argument('-v', '--verbose', default=5, type=int, help='debug message level [1-5]')
    parser.add_argument('-c', '--create', default='True', help='create dataframe from file')
    parser.add_argument('-i', '--ifile', default='NSL-KDD/KDDTrain+.txt', help='train dataset file to use')
    parser.add_argument('-t', '--test', default='NSL-KDD/KDDTest+.txt', help='test dateset file to use')
    parser.add_argument('-f', '--feature', default='ALL', choices=['ALL','SLC','SC','DROP'],help='feature set to use')
    parser.add_argument('-e', '--evol', default='None', choices=['None','GA','PS', 'FE', 'BASE'],help='Evolutionary Algorithm to use')
    parser.add_argument('-b', '--baseline', default='False', help='output malicious vectors for reference')
    parser.add_argument('-d', '--dataset', default='NSL', choices=['NSL','UNSW'],help='dataset to use: NSL or UNSW')
    args = parser.parse_args()
    
    if args.verbose == 1:
        level=logging.CRITICAL
    elif args.verbose == 2:
        level=logging.ERROR
    elif args.verbose == 3:
        level=logging.WARNING
    elif args.verbose == 4:
        level=logging.INFO
    else:
        level=logging.DEBUG
    logging.basicConfig(level=level, format='%(asctime)s - %(levelname)s -%(message)s')

    if args.dataset == 'NSL':
        dset = 0
        cols = 122
    elif args.dataset == 'UNSW':
        dset = 1
        cols = 196

    drop_amount = 2
    if args.feature == 'SLC':
        feat = 1
    elif args.feature == 'SC':
        feat = 2
    elif args.feature == 'DROP':
        feat = 3
        cols = cols - drop_amount
    else:
        feat = 0
    logging.debug('feature selection is: %d',feat)

    if args.create == 'True':
        train_df, train_lbls = read_data(args.ifile, dset)
        test_df, test_lbls = read_data(args.test, dset)
    
    proto_list, srv_list, flag_list = equalize_categories(test_df, train_df, dset)

    if args.dataset == 'NSL':
        train_df, train_lbls = mod_data(train_df, train_lbls, proto_list, srv_list, flag_list, feat)
        test_df, test_lbls = mod_data(test_df, test_lbls, proto_list, srv_list, flag_list, feat)
        orig_max_dict = {}
        dummy_max_dict = {}
   
        if (not os.path.isfile('NSL-KDD/mod_train+_pd.csv')):
            train_df.to_csv('NSL-KDD/mod_train+_pd.csv', index=False)
            test_df.to_csv('NSL-KDD/mod_test+_pd.csv', index=False)
            np.save('NSL-KDD/mod_train+_labels.npy', train_lbls)
            np.save('NSL-KDD/mod_test+_labels.npy', test_lbls)

    elif args.dataset == 'UNSW':
        train_df, train_lbls, orig_max_dict = engr_data(train_df, train_lbls, proto_list, srv_list, flag_list)
        test_df, test_lbls, dummy_max_dict = engr_data(test_df, test_lbls, proto_list, srv_list, flag_list)

        if (not os.path.isfile('UNSW_NB15/mod_train+_pd.csv')):
            train_df.to_csv('UNSW_NB15/mod_train+_pd.csv', index=False)
            test_df.to_csv('UNSW_NB15/mod_test+_pd.csv', index=False)
            np.save('UNSW_NB15/mod_train+_labels.npy', train_lbls)
            np.save('UNSW_NB15/mod_test+_labels.npy', test_lbls)
    

    #convert pandas dataframe to numpy array
    logging.info('converting datasets to numpy arrays')
    train_df.to_numpy()
    test_df.to_numpy()

    # If running on a new model, back-up existing models as this will overwrite the old ones!!
    if args.alg == 'SVM':
        logging.info("running SVM")
        default_svm(test_lbls, test_df, train_lbls, train_df)
    elif args.alg == 'NB':
        logging.info("running NB")
        default_nb(test_lbls, test_df, train_lbls, train_df)
    elif args.alg == 'KNN':
        logging.info("running KNN")
        default_knn(test_lbls, test_df, train_lbls, train_df)
    elif args.alg == 'DT':
        logging.info("running DT")
        default_dec_tree(test_lbls, test_df, train_lbls, train_df)
    elif args.alg == 'RF':
        logging.info("running RF")
        default_random_forest(test_lbls, test_df, train_lbls, train_df)
    elif args.alg == 'MLP':
        logging.info("running MLP")
        default_MLP(test_lbls, test_df, train_lbls, train_df)
    elif args.alg == 'LDA':
        logging.info("running LDA")
        default_lda(test_lbls, test_df, train_lbls, train_df)
    elif args.alg == 'LR':
        logging.info("running LR")
        default_lr(test_lbls, test_df, train_lbls, train_df)
    elif args.alg == 'GB':
        logging.info("running GB")
        default_gb(test_lbls, test_df, train_lbls, train_df)
    elif args.alg == 'QDA':
        logging.info("running QDA")
        default_qda(test_lbls, test_df, train_lbls, train_df)
    elif args.alg == 'BAG':
        logging.info("running BAG")
        default_bag(test_lbls, test_df, train_lbls, train_df)
    elif args.alg == 'VC':
        logging.info('Running Voting Classifier')
        file_name = 'Models/vc_model.sav'
        # assumes that models are already created and saved 
        svm_file_name = 'Models/svm_model.sav'
        dt_file_name = 'Models/dt_model.sav'
        nb_file_name = 'Models/nb_model.sav'
        knn_file_name = 'Models/knn_model.sav'
        #rf_file_name = 'Models/rf_model.sav'
        #mlp_file_name = 'Models/mlp_model.sav'
        #gb_file_name = 'Models/gb_model.sav'
        #lr_file_name = 'Models/lr_model.sav'
        #lda_file_name = 'Models/lda_model.sav' 
        #qda_file_name = 'Models/qda_model.sav' 
        #bag_file_name = 'Models/bag_model.sav' 


        if (not os.path.isfile(file_name)):
            logging.info('Fitting Voting Classifier')
            svm_model = joblib.load(svm_file_name)
            dt_model = joblib.load(dt_file_name)
            nb_model = joblib.load(nb_file_name)
            knn_model = joblib.load(knn_file_name)
            #rf_model = joblib.load(rf_file_name)
            #mlp_model = joblib.load(mlp_file_name)
            #gb_model = joblib.load(gb_file_name)
            #lr_model = joblib.load(lr_file_name)
            #lda_model = joblib.load(lda_file_name)
            #qda_model = joblib.load(qda_file_name)
            #bag_model = joblib.load(bag_file_name)

            vc = VotingClassifier(estimators=[('svm', svm_model), ('dt', dt_model), ('nb', nb_model), ('knn', knn_model)], voting='soft',n_jobs= -1) #,('rf', rf_model),('mlp', mlp_model), ('gb',gb_model), ('lr',lr_model), ('lda', lda_model), ('qda', qda_model),('bag', bag_model)  
            vc = vc.fit(train_df, train_lbls)
            joblib.dump(vc, file_name)
        else:
            logging.info('loading Voting Classifier')
            vc = joblib.load(file_name)
        label_predict = vc.predict(test_df)
        logging.info('score is: %f', vc.score(test_df, test_lbls))
        logging.info('\n%s',classification_report(test_lbls, label_predict))
        
        # predict class probabilities for all classifiers
        probas = vc.predict_proba(test_df)
        # get class probabilities for the first sample in the dataset
        logging.info('prob Class 0: %f, prob Class 1: %f', probas[0,0], probas[0,1])
         
    if args.evol == 'GA':
        logging.info('running genetic algorithm') 
        if dset == 0:
            if (not os.path.isfile('NSL-KDD/mal_vecs_only_test.csv')):
                mal_test_vectors = extract_malware(test_df, test_lbls,122)
            else:
                mal_test_vectors = np.loadtxt('NSL-KDD/mal_vecs_only_test.csv', delimiter=',')
        elif dset == 1:
            if (not os.path.isfile('UNSW_NB15/mal_vecs_only_test.csv')):
                mal_test_vectors = extract_malware(test_df, test_lbls,196)
            else:
                mal_test_vectors = np.loadtxt('UNSW_NB15/mal_vecs_only_test.csv', delimiter=',')
        #my_ga = GeneticAlgorithm(0.25, 0.2, 100, max_fitness=1.0)
        divs = mal_test_vectors.shape[0]/mp.cpu_count()
        processes = [mp.Process(target=ga_pool, args=(mal_test_vectors[int(x*divs):int((x+1)*divs-1),:],orig_max_dict,dset,1)) for x in range(mp.cpu_count())]
        for p in processes:
            p.start()

        for p in processes:
            p.join()
			
    elif args.evol == 'PS':
        logging.info('running particle swarm algorithm')
        if dset == 0:
            if (not os.path.isfile('NSL-KDD/mal_vecs_only_test.csv')):
                mal_test_vectors = extract_malware(test_df, test_lbls,cols)
            else:
                mal_test_vectors = np.loadtxt('NSL-KDD/mal_vecs_only_test.csv', delimiter=',')
        elif dset == 1:
            if (not os.path.isfile('UNSW_NB15/mal_vecs_only_test.csv')):
                mal_test_vectors = extract_malware(test_df, test_lbls,cols)
            else:
                mal_test_vectors = np.loadtxt('UNSW_NB15/mal_vecs_only_test.csv', delimiter=',')
        #np.savetxt("pso_results/orig_vectors.csv", mal_test_vectors, delimiter=",",newline='\n')
        divs = mal_test_vectors.shape[0]/mp.cpu_count()
        processes = [mp.Process(target=ps_pool, args=(mal_test_vectors[int(x*divs):int((x+1)*divs-1),:],dset,1, cols)) for x in range(mp.cpu_count())]
        for p in processes:
            p.start()

        for p in processes:
            p.join()

    elif args.evol == 'FE':
        logging.info('running FENCE gradient descent algorithm')
        if dset == 0:
            if (not os.path.isfile('NSL-KDD/mal_vecs_only_test.csv')):
                mal_test_vectors = extract_malware(test_df, test_lbls,cols)
            else:
                mal_test_vectors = np.loadtxt('NSL-KDD/mal_vecs_only_test.csv', delimiter=',')
        elif dset == 1:
            if (not os.path.isfile('UNSW_NB15/mal_vecs_only_test.csv')):
                mal_test_vectors = extract_malware(test_df, test_lbls,cols)
            else:
                mal_test_vectors = np.loadtxt('UNSW_NB15/mal_vecs_only_test.csv', delimiter=',')
        #np.savetxt("pso_results/orig_vectors.csv", mal_test_vectors, delimiter=",",newline='\n')
        divs = mal_test_vectors.shape[0]/mp.cpu_count()
        processes = [mp.Process(target=fe_pool, args=(mal_test_vectors[int(x*divs):int((x+1)*divs-1),:],dset,1, cols)) for x in range(mp.cpu_count())]
        for p in processes:
            p.start()

        for p in processes:
            p.join()

    elif args.evol == 'BASE':
        logging.info('running base results')
        if dset == 0:
            if (not os.path.isfile('NSL-KDD/mal_vecs_only_test.csv')):
                mal_test_vectors = extract_malware(test_df, test_lbls,122)
            else:
                mal_test_vectors = np.loadtxt('NSL-KDD/mal_vecs_only_test.csv', delimiter=',')
            mal_labels = np.full(12833, 1)
        elif dset == 1:
            if (not os.path.isfile('UNSW_NB15/mal_vecs_only_test.csv')):
                mal_test_vectors = extract_malware(test_df, test_lbls,196)
            else:
                mal_test_vectors = np.loadtxt('UNSW_NB15/mal_vecs_only_test.csv', delimiter=',')
            mal_labels = np.full(45332, 1)
        file_name = 'Models/vc_model.sav'
        logging.info('loading Voting Classifier model')
        vc = joblib.load(file_name)
        label_predict = vc.predict(mal_test_vectors)
        logging.info('score is: %f', vc.score(mal_test_vectors, mal_labels))
        logging.info('\n%s',classification_report(mal_labels, label_predict))
        tn, fp, fn, tp = confusion_matrix(mal_labels, label_predict).ravel()
        logging.info('tn: %d, tn: %d, fp: %d, fn: %d', tn, fp, fn, tp)

    if args.baseline == 'True':
        logging.info('running baseline malicious vectors')
        if dset == 0:
            mal_test_vectors = extract_malware(test_df, test_lbls,122)
            file_name = 'NSL-KDD/mal_vecs_only_test.csv'
        elif dset == 1:
            mal_test_vectors = extract_malware(test_df, test_lbls,196)
            file_name = 'UNSW_NB15/mal_vecs_only_test.csv'
        np.savetxt(file_name, mal_test_vectors,delimiter=',', fmt='%f',newline='\n')
        
        
    logging.info('Done')

if __name__ == "__main__":
    main(sys.argv[1:])
