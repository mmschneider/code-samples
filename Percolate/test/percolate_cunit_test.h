#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../src/arralloc.h"
#include "../src/percolate.h"
#include "../src/uni.h"
#include "CUnit/Automated.h"
#include "CUnit/Basic.h"

/**
 * @brief Initialises test suite.
 * @return 0 if run without errors.
 */
int initialise_suite(void);

/**
 * @brief Cleans up test suite.
 @ return 0 if run without errors.
 */
int cleanup_suite(void);

/**
 * @brief Check that default value is used if a non postive dimension input is
 * given.
 */
void cluster_size_same_test(void);

/**
 * @brief Checks that two cluster with the same size will be compared by rank.
 */
void cluster_size_same_test(void);

/*
 * @brief Checks that two cluster with the different size will be compared by
 * size.
 */
void cluster_size_diff_test(void);

/*
 * @brief Checks that map initialization returns the expected density.
 */
void check_density(void);

/*
 * @brief Checks that map initialization puts zeros in border.
 */
void check_border(void);

/*
 * @brief Checks that map initialization works with map values initialized and
 * uninitialized.
 */
void check_when_values_declared(void);

/*
 * @brief Checks that clusters are created correctly for a map that percolates.
 */
void check_cluster_creation_map_percolates(void);

/*
 * @brief Checks that clusters are created correctly for a map that does not
 * percolate.
 */
void check_cluster_creation_map_not_percolates(void);

/*
 * @brief Checks that check_percolate correctly identifies a map that
 * percolates.
 */
void check_does_percolate(void);

/*
 *  * @brief Checks that check_percolate correctly identifies a map that does
 * not percolate.
 *   */
void check_does_not_percolate(void);

/*
 * @brief Checks that write_datafile correctly writes a map.
 */
void check_dat_file(void);

/*
 * @brief Check that set_rank properly switches map property
 * max_number_of_clusters to number of clusters when max_number_of_clusters is
 * higher than the number of clusters.
 */
void check_max_num_cluster_greater(void);

/*
 * @brief Check that set_rank does not switch max_number_of_clusters when it is
 * less than or equal to the number of clusters.
 */
void check_max_num_cluster_no_change(void);

/*
 * @brief Checks that set_rank returns 0 for a map with no clusters.
 */
void check_number_of_clusters_zero(void);

/*
 * @brief Checks that set_rank returns 2 for a map with 2 clusters.
 */
void check_number_of_clusters(void);

/*
 * @brief Checks that set_rank gets the correct max cluster size for one
 * distinct max sized cluster.
 */
void check_max_size_one(void);

/*
 * @brief Checks that set_rank gets the correct max cluster size even if two
 * clusters have that size.
 */
void check_max_size_duplicate(void);

/*
 * @brief Check that set_rank correctly creates rank array when all clusters
 * have disintct sizes.
 */
void check_rank_distinct_sizes(void);

/*
 * @brief Check that set_rank correctly handles when two clusters have the max
 * cluster size.
 */
void check_rank_same_max(void);

/*
 * @brief Checks that write_percfile correctly writes a map.
 */
void check_perc_file(void);

/*
 * @brief Checks that write_percfile correctly writes a map with no clusters.
 */
void check_perc_file_no_clusters(void);

/*
 * @brief Compares arrays up to element size.
 * @param array_1 A pointer to the first array.
 * @param array_2 A pointer to the second array.
 * @size The number of elements per array that will be compared.
 * @returns The number of elements that were different.
 */
int compare_arrays(int *array_1, int *array_2, int size);

/*
 * @brief Gives a map specific values.
 * @param map The map struct to update.
 * @param values A pointer to the array of values to update map.values to.
 * @return 0 if run without errors.
 */
int set_map_values(struct map *map, int *values);

/*
 * @brief Creates a test map to run tests on.
 * @param dimension The length and width dimension of the map.
 * @param density The density of filled square with a value between 0 and 1.
 * @return map The map to run tests on.
 */
struct map make_test_map(int dimension, float density);



