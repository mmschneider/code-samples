#include "percolate_cunit_test.h"
#include <ctype.h>

int initialise_suite(void) { return 0; }

int cleanup_suite(void) { return 0; }

/* cluster_compare suite. */

void cluster_size_same_test(void) {
  struct cluster c1;
  struct cluster c2;
  // Create clusters with different size so comparision is done based on size.
  c1.id = 2;
  c1.size = 2;
  c2.id = 3;
  c2.size = 2;
  int compare;
  compare = cluster_compare(&c1, &c2);
  CU_ASSERT_EQUAL(compare, 1);
}

void cluster_size_diff_test(void) {
  struct cluster c1;
  struct cluster c2;
  // Create clusters with same size so comparision is done based on rank.
  c1.id = 2;
  c1.size = 4;
  c2.id = 3;
  c2.size = 2;
  int compare;
  compare = cluster_compare(&c1, &c2);
  CU_ASSERT_EQUAL(compare, -2);
}

/* initialize_map suite. */

void check_density(void) {
  int i, j, seed, not_filled;
  seed = 1564;
  rinit(seed);
  struct map map = make_test_map(20, .4);
  initialize_map(map);

  // Create an array of 0's for comparision.
  int zeros[22 * 22] = {0};

  // Get the number of element in map that are not zero.
  not_filled = compare_arrays(map.values[0], zeros,
                              (map.dimension + 2) * (map.dimension + 2));
  float actual_density =
      1.0 - ((double)not_filled) / ((double)map.dimension * map.dimension);
  CU_ASSERT_DOUBLE_EQUAL(actual_density, 0.425, .00001);
}

void check_border(void) {
  int i, seed;
  seed = 1564;
  rinit(seed);
  struct map map = make_test_map(20, .4);
  initialize_map(map);
  // Check that all border values are 0.
  for (i = 0; i <= map.dimension + 1; i++) {
    CU_ASSERT_EQUAL(map.values[0][i], 0);
    CU_ASSERT_EQUAL(map.values[map.dimension + 1][i], 0);
    CU_ASSERT_EQUAL(map.values[i][0], 0);
    CU_ASSERT_EQUAL(map.values[i][map.dimension + 1], 0);
  }
}

void check_when_values_declared(void) {
  int seed = 1564;
  struct map map = make_test_map(5, .4);
  struct map compare_map = make_test_map(5, .4);

  // Declare map values all to 1.
  int map_values[20] = {1};
  set_map_values(&map, map_values);
  rinit(seed);
  initialize_map(map);

  // Do not declare the values for comapre_map.
  rinit(seed);
  initialize_map(compare_map);

  CU_ASSERT_EQUAL(0, compare_arrays(map.values[0], compare_map.values[0],
                                    map.dimension + 2));
}

/* create_clusters suite. */

void check_cluster_creation_map_percolates(void) {
  struct map map = make_test_map(3, .4);
  int values[25] = {0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 2, 3,
                    0, 0, 0, 0, 4, 5, 0, 0, 0, 0, 0, 0};
  set_map_values(&map, values);

  // The values we expect map.values to update to after create_clusters.
  int updated_values[25] = {0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 5, 5,
                            0, 0, 0, 0, 5, 5, 0, 0, 0, 0, 0, 0};

  create_clusters(map);
  CU_ASSERT_EQUAL(
      0, compare_arrays(updated_values, map.values[0], map.dimension + 2));
}

void check_cluster_creation_map_not_percolates(void) {
  struct map map = make_test_map(3, .4);
  int values[25] = {0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 2, 0,
                    3, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0};
  set_map_values(&map, values);

  // The values we expect map.values to update to after create_clusters.
  int updated_values[25] = {0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 2, 0,
                            3, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0};

  create_clusters(map);
  CU_ASSERT_EQUAL(
      0, compare_arrays(updated_values, map.values[0], map.dimension + 2));
}

/* check_percolate suite. */

void check_does_percolate(void) {
  struct map map = make_test_map(3, .4);
  int values[25] = {0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 5, 5,
                    0, 0, 0, 0, 5, 5, 0, 0, 0, 0, 0, 0};
  set_map_values(&map, values);
  CU_ASSERT_EQUAL(check_percolate(map), 1);
}

void check_does_not_percolate(void) {
  struct map map = make_test_map(3, .4);
  int values[25] = {0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 2, 0,
                    3, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0};
  set_map_values(&map, values);
  CU_ASSERT_EQUAL(check_percolate(map), 0);
}

/* write_data_file suite. */

void check_dat_file(void) {
  FILE *fp;
  char *datafile = "map_test.dat";
  struct map map = make_test_map(3, .4);
  int values[25] = {0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 5, 5,
                    0, 0, 0, 0, 5, 5, 0, 0, 0, 0, 0, 0};
  set_map_values(&map, values);

  // The values we expect to see when we read from map_test.dat.
  int expected_write_values[9] = {0, 0, 5, 0, 5, 5, 5, 5, 0};
  // An array to hold the values read in from map_test.dat.
  int check_values[25];
  write_datafile(map, datafile, fp);

  fp = fopen(datafile, "r");
  int i = 0;
  char c;
  // Read through map_test.dat character by character, building the check_values
  // array when a digit is seen.
  while (EOF != (c = fgetc(fp))) {
    if (isdigit(c)) {
      // Subtract ASCII code to get numeric value represented by character
      check_values[i] = (int)(c - '0');
      i++;
    }
  }
  fclose(fp);
  // Test if check_values and expected_write_values are equal.
  CU_ASSERT_EQUAL(0, compare_arrays(check_values, expected_write_values, 9));
}

/* set_rank suite. */

void check_max_num_cluster_greater(void) {
  int *rank;
  int max_size;
  struct cluster *cluster_list;
  struct map map = make_test_map(3, .4);
  cluster_list = (struct cluster *)arralloc(sizeof(struct cluster), 1,
                                            map.dimension * map.dimension);
  rank = (int *)arralloc(sizeof(int), 1, map.dimension * map.dimension);
  int values[25] = {0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 5, 5,
                    0, 0, 0, 0, 5, 5, 0, 0, 0, 0, 0, 0};
  set_map_values(&map, values);
  int number_of_clusters = set_rank(&map, cluster_list, &rank, &max_size);

  // Test that map.max_number_of_clusters is number_of_clusters, as exepcted.
  CU_ASSERT_EQUAL(map.max_number_of_clusters, number_of_clusters);
}

void check_max_num_cluster_no_change(void) {
  int *rank;
  int max_size;
  struct cluster *cluster_list;
  struct map map = make_test_map(3, .4);
  cluster_list = (struct cluster *)arralloc(sizeof(struct cluster), 1,
                                            map.dimension * map.dimension);
  rank = (int *)arralloc(sizeof(int), 1, map.dimension * map.dimension);
  int values[25] = {0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 2, 3,
                    0, 0, 0, 0, 4, 5, 0, 0, 0, 0, 0, 0};
  set_map_values(&map, values);
  map.max_number_of_clusters = 3;
  set_rank(&map, cluster_list, &rank, &max_size);

  // Test that map.max_number_of_clusters is 3, as exepcted.
  CU_ASSERT_EQUAL(map.max_number_of_clusters, 3);
}

void check_number_of_clusters_zero(void) {
  int *rank;
  int max_size;
  struct cluster *cluster_list;
  struct map map = make_test_map(1, .4);
  cluster_list = (struct cluster *)arralloc(sizeof(struct cluster), 1,
                                            map.dimension * map.dimension);
  rank = (int *)arralloc(sizeof(int), 1, map.dimension * map.dimension);
  int values[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
  set_map_values(&map, values);
  int number_of_clusters = set_rank(&map, cluster_list, &rank, &max_size);

  // Test that number_of_clusters is 2, as exepcted.
  CU_ASSERT_EQUAL(0, number_of_clusters);
}

void check_number_of_clusters(void) {
  int *rank;
  int max_size;
  struct cluster *cluster_list;
  struct map map = make_test_map(2, .4);
  cluster_list = (struct cluster *)arralloc(sizeof(struct cluster), 1,
                                            map.dimension * map.dimension);
  rank = (int *)arralloc(sizeof(int), 1, map.dimension * map.dimension);
  int values[16] = {0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0};
  set_map_values(&map, values);
  int number_of_clusters = set_rank(&map, cluster_list, &rank, &max_size);

  // Test that number_of_clusters is 2, as exepcted.
  CU_ASSERT_EQUAL(2, number_of_clusters);
}

void check_max_size_one(void) {
  int *rank;
  int max_size;
  struct cluster *cluster_list;
  struct map map = make_test_map(3, .4);
  cluster_list = (struct cluster *)arralloc(sizeof(struct cluster), 1,
                                            map.dimension * map.dimension);
  rank = (int *)arralloc(sizeof(int), 1, map.dimension * map.dimension);
  int values[25] = {0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 2,
                    0, 0, 0, 0, 3, 3, 0, 0, 0, 0, 0, 0};
  set_map_values(&map, values);
  set_rank(&map, cluster_list, &rank, &max_size);

  // Test that max_size is 2, as exepcted.
  CU_ASSERT_EQUAL(2, max_size);
}

void check_max_size_duplicate(void) {
  int *rank;
  int max_size;
  struct cluster *cluster_list;
  struct map map = make_test_map(3, .4);
  cluster_list = (struct cluster *)arralloc(sizeof(struct cluster), 1,
                                            map.dimension * map.dimension);
  rank = (int *)arralloc(sizeof(int), 1, map.dimension * map.dimension);
  int values[25] = {0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0,
                    0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0};
  set_map_values(&map, values);
  set_rank(&map, cluster_list, &rank, &max_size);

  // Test that max_size is 2, as exepcted.
  CU_ASSERT_EQUAL(2, max_size);
}

void check_rank_distinct_sizes(void) {
  int *rank;
  int i;
  int max_size;
  struct cluster *cluster_list;
  struct map map = make_test_map(3, .4);
  cluster_list = (struct cluster *)arralloc(sizeof(struct cluster), 1,
                                            map.dimension * map.dimension);
  rank = (int *)arralloc(sizeof(int), 1, map.dimension * map.dimension);
  int values[25] = {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 2, 2,
                    2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0};
  set_map_values(&map, values);

  // The values we expect rank to hold.
  int check_values[25] = {1, 0, 2};
  set_rank(&map, cluster_list, &rank, &max_size);

  CU_ASSERT_EQUAL(0, compare_arrays(rank, check_values, 3));
}

void check_rank_same_max(void) {
  int *rank;
  int i;
  int max_size;
  struct cluster *cluster_list;
  struct map map = make_test_map(3, .4);
  cluster_list = (struct cluster *)arralloc(sizeof(struct cluster), 1,
                                            map.dimension * map.dimension);
  rank = (int *)arralloc(sizeof(int), 1, map.dimension * map.dimension);

  int values[25] = {0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 2,
                    2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0};
  set_map_values(&map, values);

  // The values we expect rank to hold.
  int check_values[25] = {1, 0, 2};
  set_rank(&map, cluster_list, &rank, &max_size);

  CU_ASSERT_EQUAL(0, compare_arrays(rank, check_values, 3));
}

/* write_percfile suite. */

void check_perc_file(void) {
  FILE *fp;
  char *percfile = "map_test.pgm";
  struct map map = make_test_map(3, .4);
  int values[25] = {0, 0, 0, 0, 0, 0, 1, 2, 0, 0, 0, 2, 2,
                    0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0};
  set_map_values(&map, values);

  // The values we expect to see when we read from map_test.pgm.
  char expected_write_values[] =
      "P2\n3 3\n2\n    2    2    2\n    0    0    2\n    1    0    0\n";
  write_percfile(map, percfile, fp);
 
  // An array to hold the values read in from map_test.pgm.
  char check_values[strlen(expected_write_values)];
 
  fp = fopen(percfile, "r");
  int i = 0;
  char c;
  // Read through map_test.pgm character by character, building the check_values
  // array.
  while (EOF != (c = fgetc(fp))) {
    check_values[i] = c;
    i++;
  }
  fclose(fp);
  // Test if check_values and expected_write_values are equal.
  CU_ASSERT_EQUAL(0, strcmp(check_values, expected_write_values));
}

void check_perc_file_no_clusters(void) {
  FILE *fp;
  char *percfile = "map_test.pgm";
  struct map map = make_test_map(3, .4);
  int values[25] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  set_map_values(&map, values);

  // The values we expect to see when we read from map_test.pgm.
  char expected_write_values[] =
      "P2\n3 3\n1\n    0    0    0\n    0    0    0\n    0    0    0\n";
  write_percfile(map, percfile, fp);
  
  // An array to hold the values read in from map_test.pgm.
  char check_values[strlen(expected_write_values)];
  
  fp = fopen(percfile, "r"); 
  int i = 0;
  char c;
  // Read through map_test.pgm character by character, building the check_values
  // array.
  while (EOF != (c = fgetc(fp))) {
    check_values[i] = c;
    i++;
  }
  fclose(fp);
  // Test if check_values and expected_write_values are equal.
  CU_ASSERT_EQUAL(0, strcmp(check_values, expected_write_values));
}

/* Helpful tester functions. */

int compare_arrays(int *array_1, int *array_2, int size) {
  int i, diff;
  diff = 0;
  for (i = 0; i < size; i++) {
    // Get value of each element in an array by moving the pointer.
    if (*(array_1 + i) != *(array_2 + i)) {
      diff++;
    }
  }
  // return the number of elements that were different between the two arrays.
  return diff;
}

int set_map_values(struct map *map, int *values) {
  int i, j;
  for (i = 0; i < map->dimension + 2; i++) {
    for (j = 0; j < map->dimension + 2; j++) {
      // update the map value elements by moving the pointer to the values
      // array.
      map->values[i][j] = *(values + i * (map->dimension + 2) + j);
    }
  }
  return 0;
}

struct map make_test_map(int dimension, float density) {
  struct map map;
  map.dimension = dimension;
  map.rho = density;
  map.values = (int **)arralloc(sizeof(int), 2, dimension + 2, dimension + 2);
  map.max_number_of_clusters = dimension * dimension;
  return map;
}
