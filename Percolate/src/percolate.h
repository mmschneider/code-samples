/**
 * @file percolate.h
 * @author B160489
 * @brief Header file for percolate.
 * 
 */

#include <argp.h>

/**
 * Used by main to communicate with parse_opt.
 */
struct arguments {
  int dimension; /**< The length and width dimension of the map. */
  int max_number_of_clusters; /**< The maximum number of clusters to consider. */
  int seed; /**< The seed for the random number generator. */
  float density; /**< The density if filled square with a value between 0 and 1. */
  char *datafile; /**< The name of the output .dat file. */
  char *percfile; /**< The name of the output .pgm file. */
};

/**
 * The options that the program understands.
 */
static struct argp_option options[] = {
    {"dimension", 'd', "SIZE", 0, "The length and width dimension of the map."},
    {"max_number_of_clusters", 'm', "MAX", 0,
     "The max number of clusters to show on the map."},
    {"seed", 's', "SEED", 0, "The seed for the random number generator."},
    {"density", 'r', "RHO", 0, "The density of filled square with a value between 0 and 1."},
    {"datafile", 'f', "DATA_FILE", 0, "The name of the output .dat file."},
    {"percfile", 'p', "PERC_FILE", 0, "The name of the output .pgm file."},
    {0}};

/**
 * A structure with information about a cluster.
 */
struct cluster {
  int id; /**< The unique id of the cluster. */
  int size; /**< The number of squares in the cluster. */
};

/**
 * A structure with information about the map.
 */
struct map {
  int** values; /**< The array holding the values of the spaces on the map */
  int dimension; /**< The length and width dimension of the map. */
  float rho; /**< The density of filled square with a value between 0 and 1. */
  int max_number_of_clusters; /**< The max number of clusters to consider. */
};

/**
 * @brief Determines size difference between clusters
 *
 * Used in qsort to determine which of two clusters is bigger. 
 * If clusters are the same size, determines which cluster is
 * larger based on the larger cluster id.
 *
 * @param *p1 The pointer to the first cluster.
 * @param *p2 The pointer to the second cluster.
 * @return The difference between the two cluster sizes.
 */
int cluster_compare(const void *p1, const void *p2);

/**
 * @brief Initializes the map.
 *
 * Based on the random number generator, determine which
 * spaces on the map will be filled (0) and which will be 
 * empty (unique id).
 *
 * @param map The map struct.
 * @return 0 if run without errors.
 */
int initialize_map(struct map map);

/**
 * @brief Combine neighbors to create clusters.
 *
 * Iterates through the map, combining neighbors into single
 * clusters by replacing each square with the maximum of itself
 * and it's for neighbors. 
 *
 * @param map The map struct.
 * @return 0 if run without errors.
 */
int create_clusters(struct map map);

/**
 * @brief Determine if any of the clusters percolate.
 *
 * See if the id of a cluster exists on the top and bottom
 * row. If it does, then the map percolates.
 *
 * @param map The map struct.
 * @return 0 if run without errors.
 */
int check_percolate(struct map map);

/**
 * @brief Writes information to the .dat file
 *
 * Writes a plain text representation of the final state
 * of the map.
 *
 * @ param map The map struct.
 * @ param *datafile Name of the .dat file.
 * @ param *fp File handle.
 * @return 1 if percolates, 0 if does not percolate.
 */
int write_datafile(struct map map, char *datafile, FILE *fp);

/**
 * @brief Determines each cluster's rank for printing.
 *
 * Finds the size of each cluster, determines to max number of clusters to show,
 * and sets the rank of the cluster based on it's size order.
 *
 * @ param *map The map struct.
 * @ param cluster_list Array of clusters.
 * @ param rank Array holding rank of each cluster.
 * @ param max_size Pointer to value of the largest cluster's size. 
 * @return The max number of clusters to show.
 */
int set_rank(struct map *  map, struct cluster *cluster_list, int **rank, int *max_size);

/**
 * @brief Writes information to the .pgm file.
 *
 * Writes an image file that colours the cluster. the largest cluster will
 * be black. Other clusters will be shades of grey, the smaller the cluster 
 * the lighter the shade.
 *
 * @ param map The map struct.
 * @ param percfile Name of the .pgm file.
 * @ param fp File handle.
 * @return 0 if run without errors.
 */
int write_percfile(struct map map, char *percfile, FILE *fp);
