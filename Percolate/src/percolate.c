#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "arralloc.h"
#include "percolate.h"
#include "uni.h"


int cluster_compare(const void *p1, const void *p2) {
  int size1, size2, id1, id2;

  /* Get sizes and ids from cluster. */
  size1 = ((struct cluster *)p1)->size;
  size2 = ((struct cluster *)p2)->size;

  id1 = ((struct cluster *)p1)->id;
  id2 = ((struct cluster *)p2)->id;

  /* Find difference in size. */
  if (size1 != size2) {
    return (size2 - size1);
  } else {
    return (id2 - id1);
  }
}

int initialize_map(struct map map) {

  int not_filled;
  int i, j;
  float r;

  /* Randomly set a subset of the elements based on rho to a unique int. */
  not_filled = 0;
  for (i = 0; i <= map.dimension + 1; i++) {
    for (j = 0; j <= map.dimension + 1; j++) {
      /* Inside map check if element should be filled or not. */
      if (i <= map.dimension && i >= 1 && j <= map.dimension && j >= 1) {
        r = random_uniform();
        /* if within rho give space a unique int value. */
        if (r > map.rho) {
          not_filled++;
          map.values[i][j] = not_filled;
        }
        /* Otherwise fill in the space. */
        else {
          map.values[i][j] = 0;
        }
      }
      /* Outside map, set halo spaces to filled. */
      else {
        map.values[i][j] = 0;
      }
    }
  }

  printf("rho = %f, actual density = %f\n", map.rho,
         1.0 - ((double)not_filled) / ((double)map.dimension * map.dimension));
  return 0;
}

int create_clusters(struct map map) {
  int loop, number_of_changes, largest, j, i;
  loop = 1;
  number_of_changes = 1;

  /* If it is possible there might be more cluster combinations go through the loop. */
  while (number_of_changes > 0) {
    number_of_changes = 0;
    for (i = 1; i <= map.dimension; i++) {
      for (j = 1; j <= map.dimension; j++) {
        /* For spaces that are not filled replace with the maximum of itself or its four neighbors */
        if (map.values[i][j] != 0) {
          largest =
              fmax(fmax(fmax(map.values[i - 1][j], map.values[i + 1][j]), map.values[i][j - 1]),
                   map.values[i][j + 1]);
          if (largest > map.values[i][j]) {
            map.values[i][j] = largest;
            number_of_changes++;
          }
        }
      }
    }
    printf("Number of changes on loop %d is %d\n", loop, number_of_changes);
    loop++;
  }
  return 0;
}

int check_percolate(struct map map) {
  int top_row_index, bottom_row_index, percolate_cluster_number;
  int percolates = 0;

  /* Iterate through the top row looking for an unfill space/ */
  for (top_row_index = 1; top_row_index <= map.dimension; top_row_index++) {
    if (map.values[top_row_index][map.dimension] > 0) {

      /* For each unfilled space see if that spaces value exists in the bottom row. */
      for (bottom_row_index = 1; bottom_row_index <= map.dimension;
           bottom_row_index++) {
        if (map.values[top_row_index][map.dimension] == map.values[bottom_row_index][1]) {
          /* If it does, then the cluster percolates. */
          percolates = 1;
          percolate_cluster_number = map.values[top_row_index][map.dimension];
        }
      }
    }
  }

  if (percolates) {
    printf("Cluster DOES percolate. Cluster number: %d\n", percolate_cluster_number);
    return 1;
  } else {
    printf("Cluster DOES NOT percolate\n");
    return 0;
  }
}

int write_datafile(struct map map, char *datafile, FILE *file_pointer) {
  int i, j;
  printf("Opening file <%s>\n", datafile);
  file_pointer = fopen(datafile, "w");
  printf("Writing data ...\n");

  /* Write contents of the map to the datafile. */
  for (j = map.dimension; j >= 1; j--) {
    for (i = 1; i <= map.dimension; i++) {
      fprintf(file_pointer, " %4d", map.values[i][j]);
    }
    fprintf(file_pointer, "\n");
  }

  printf("...done\n");
  fclose(file_pointer);
  printf("File closed\n");
  return 0;
} 


set_rank(struct map *  map, struct cluster *cluster_list, int **rank, int *max_size){
  int i, j, number_of_clusters;
   /* Initialize cluster_list and rank. */
   for (i = 0; i < map->dimension * map->dimension; i++) {
    (*rank)[i] = -1;
    cluster_list[i].size = 0;
    cluster_list[i].id = i + 1;
  }

  /* Find size of each cluster */
  for (i = 1; i <= map->dimension; i++) {
    for (j = 1; j <= map->dimension; j++) {
      if (map->values[i][j] != 0) {
        ++(cluster_list[map->values[i][j] - 1].size);
      }
    }
  }

  /* Sort the cluster_list using cluster_compare to compare elements. */
  qsort(cluster_list, (size_t)map->dimension * map->dimension, sizeof(struct cluster),
        cluster_compare);

  /* Get the largest cluster */
  *max_size = cluster_list[0].size;
  for (number_of_clusters = 0;
       number_of_clusters < map->dimension * map->dimension && cluster_list[number_of_clusters].size > 0;
       number_of_clusters++)
  ; //End of for loop. No body.
  if (map->max_number_of_clusters > number_of_clusters) {
    map->max_number_of_clusters = number_of_clusters;
  }

  /* Set rank based on size order of cluster */
  for (i = 0; i < number_of_clusters; i++) {
    (*rank)[cluster_list[i].id - 1] = i;
  }
  return number_of_clusters;
}

int write_percfile(struct map map,
                   char *percfile, FILE *file_pointer) {
  int number_of_clusters, max_size, i, j;
  struct cluster *cluster_list;
  int colour;
  int *rank;
  cluster_list = (struct cluster *)arralloc(sizeof(struct cluster), 1,
                                         map.dimension * map.dimension);
  rank = (int *)arralloc(sizeof(int), 1, map.dimension * map.dimension);

  number_of_clusters = set_rank(&map, cluster_list, &rank, &max_size);

  printf("Opening file <%s>\n", percfile);
  file_pointer = fopen(percfile, "w");
  printf("Map has %d clusters, maximum cluster size is %d\n", number_of_clusters,
         max_size);
  if (map.max_number_of_clusters == 1) {
    printf("Displaying the largest cluster\n");
  } else if (map.max_number_of_clusters == number_of_clusters) {
    printf("Displaying all clusters\n");
  } else {
    printf("Displaying the largest %d clusters\n", map.max_number_of_clusters);
  }
  printf("Writing data ...\n");

  fprintf(file_pointer, "P2\n");
  if (map.max_number_of_clusters > 0) {
    fprintf(file_pointer, "%d %d\n%d\n", map.dimension, map.dimension, map.max_number_of_clusters);
  } else {
    fprintf(file_pointer, "%d %d\n%d\n", map.dimension, map.dimension, 1);
  }

  /* Colour squares based on rank. */
  for (j = map.dimension; j >= 1; j--) {
    for (i = 1; i <= map.dimension; i++) {
      colour = map.max_number_of_clusters;
      if (map.values[i][j] > 0 && rank[map.values[i][j] - 1] < map.max_number_of_clusters) {
        colour = rank[map.values[i][j] - 1];
      }
      fprintf(file_pointer, " %4d", colour);
    }
    fprintf(file_pointer, "\n");
  }

  printf("...done\n");
  fclose(file_pointer);
  printf("File closed\n");
  free(cluster_list);
  free(rank);
  return 0;
}


