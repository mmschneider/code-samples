#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "arralloc.h"
#include "percolate.h"
#include "uni.h"

/**
 * @brief Parse a single option.
 *
 * This looks for option flags given in the command line
 * and parses that input to allow users to input values for dimensions,
 * max_number_of_clusters, seed, rho, datafile, and percfile.
 *
 * @return 0 if run with no errors.
 */
static int parse_opt(int key, char *arg, struct argp_state *state) {
  /* Get the input argument from argp_parse, which is a pointer to our arguments
 *    * structure*/
  struct arguments *arguments = state->input;

  switch (key) {
  case 'd': {
    int input_dimension = atoi(arg);
    if (input_dimension <= 0){
      printf("The dimension must be a positive integer. Using default value of 20. \n");
    } else {
      arguments->dimension = input_dimension;
    }
    break;
  }
  case 'm': {
    int input_max_number_of_clusters = atoi(arg);
    if (input_max_number_of_clusters < 0){
      printf("The max number of clusters must be a non-negative integer. Using default value. \n");
    } else {
      arguments->max_number_of_clusters = input_max_number_of_clusters;
    }
    break;
  }
  case 's': {
    int input_seed = atoi(arg);
    if (input_seed < 0 || input_seed > 900000000){
      printf("The seed must be a non-negative integer less than 900000000. Using default value of 1564. \n");
    } else {
      arguments->seed = input_seed;
    }
    break;
  }
  case 'r': {
    float input_rho = atof(arg);
    if (input_rho < 0 || input_rho > 1) {
      printf("Density must be between 0 and 1. Using default value of .4. \n");
    } else {
      arguments->density = input_rho;
    }
    break;
  }
  case 'f': {
    char *end = ".dat";
    if (strlen(arg) < 5 || strcmp(end, arg + strlen(arg) - 4) != 0) {
      printf("Data file must be of form <filename>.dat. Using default value of map.dat. \n");
    } else {
      arguments->datafile = arg;
    }
    break;
  }
  case 'p': {
    char *end = ".pgm";
    if (strlen(arg) < 5 || strcmp(end, arg + strlen(arg) - 4) != 0) {
      printf("Perc file must be of form <filename>.pgm. Using default value of "
             "perc.pgm. \n");
    } else {
      arguments->percfile = arg;
    }
    break;
  }
  }
  return 0;
}

static struct argp argp = {options, parse_opt, NULL, NULL};

int main(int argc, char **argv) {
  
  int i, j;
  struct map map;
  int seed;
  char *datafile;
  char *percfile;
  struct arguments arguments;

  /* Set default options */

  arguments.dimension = 20;
  arguments.density = 0.40;
  arguments.seed = 1564;
  arguments.datafile = "map.dat";
  arguments.percfile = "map.pgm";
  arguments.max_number_of_clusters = arguments.dimension * arguments.dimension;

  /* Receive options from the command line if they were given */

  argp_parse(&argp, argc, argv, 0, 0, &arguments);

  map.dimension = arguments.dimension;
  map.values = (int **)arralloc(sizeof(int), 2, map.dimension + 2, map.dimension + 2);
  map.rho = arguments.density;
  seed = arguments.seed;
  datafile = arguments.datafile;
  percfile = arguments.percfile;
  if(arguments.max_number_of_clusters > map.dimension * map.dimension)
  {
    map.max_number_of_clusters = map.dimension * map.dimension;
  }
  else
  {
    map.max_number_of_clusters = arguments.max_number_of_clusters;
  }
  FILE *file_pointer;

  /* Use seed to set state for random number generator */

  rinit(seed);

  printf("Parameters are rho=%f, dimension=%d, seed=%d\n", map.rho, map.dimension,
         seed);

  initialize_map(map);

  create_clusters(map);

  check_percolate(map);

  write_datafile(map, datafile, file_pointer);

  write_percfile(map, percfile, file_pointer);

  free(map.values);
}
