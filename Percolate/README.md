# Programming-Skills-Course-Work

## About this program ##

The program creates a grid. Each square is randomly assigned to be empty (1) or filled (0) according to the
designed density distribution of filled squares. Each empty square is then assigned a unique number. The
program iteratively updates the grid, updating each square is updated with the maximum of its neighbours.
When an iteration results in no squares being updated, the iteration exits. The program then checks whether
there is a cluster on the top row of the grid that has the same number as one on the bottom row i.e. it checks
whether percolation has occurred. 

## How to run ##
1. Run the appropriate make command.
   * run_percolate: Compiles binaries into an executable. Add OPT=flag to use a gcc optimize flag.
   * clean: Removes the executables and map output files. 
   * all: Runs clean followed by run_percolate and test.
   * test: Compiles, assembles, and links binaries to create code for unit tests. Runs tests.
   * help: Provides summary of make commands.
2. Run ./run_percolate <additional options>.
   * ./run_percolate --usage
     * Shows usage for ./run_percolate.
   * ./run_percolate --help
     * Prints information about options.


***The program was run on Cirrus and built from C code. It was compilled using gcc and the standard C math library.***

**Options**
  * <--dimension=|-d>
    * The length and width dimension of the map.
  * <--max_number_of_clusters=|-m>
    * The max number of clusters to consider.
  * <--seed=|-s>
    * The seed for the random number generator.
  * <--density=|-r>
    * The density of filled square with a value between 0 and 1.
  * <--datafile=|-d>
    * The name of the output .dat file.
  * <--percfile=|-p>
    * The name of the output .pgm file.

## Documentation ##
* Run doxygen doc_config to create html and latex documentation.

## Running CUnit Tests on Circus ##
* First Time
  1. Download CUnit-2.1-2-src.tar.bz2 from http://downloads.sourceforge.net/project/cunit/CUnit/2.1-2/CUnit-2.1-2-
src.tar.bz2.
  2. Unpack.
  3. cd into CUnit directory.
  4. Run ./configure --prefix=$HOME.
  5. Run make.
  6. Run make install.
* Every time you log onto Cirrus.
  1. Run export C\_INCLUDE\_PATH=$HOME/include:$C\_INCLUDE\_PATH.
  2. Run export LIBRARY\_PATH=$HOME/lib:$LIBRARY\_PATH.
  3. Run export LD\_LIBRARY\_PATH=$HOME/lib:$LD\_LIBRARY\_PATH. 

